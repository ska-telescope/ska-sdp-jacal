## JACAL
**J**oint **A**stronomy **CAL**ibration and
imaging software
<img align="right" src="logo.jpg" alt="logo" width="265"/>

[![Documentation Status](https://readthedocs.org/projects/ska-telescope-sdp-jacal/badge/?version=latest)](https://ska-telescope-sdp-jacal.readthedocs.io/en/latest/?badge=latest)

JACAL integrates [ASKAPSoft](https://www.atnf.csiro.au/computing/software/askapsoft/sdp/docs/current/pipelines/introduction.html)
(now rebranded to *Yandasoft*)
and the execution framework [DALiuGE](https://github.com/ICRAR/daliuge).
A shared library offers a calling convention supported by DALiuGE and internally links and reuses ASKAPSoft code.
JACAL is freely available in this [GitLab repository](https://gitlab.com/ska-telescope/ska-sdp-jacal)
under a variation of the open source BSD 3-Clause [License](LICENSE).
The repository contains the following:
* The C/C++ code of the shared library libjacal.so described above.
* A number of tests running the different components inside DALiuGE graphs.
* A standalone utility for library testing independent of DALiuGE.

This repository is an offshoot from the original located in [GitHub](https://github.com/ICRAR/jacal).
The latter should be considered deprecated, and has only been left available
for reference.

## Installation

Installation instructions can be found in jacal's [documentation](https://ska-telescope-sdp-jacal.readthedocs.io/en/latest/?badge=latest).

## Acknowledgement

The development of JACAL was jointly initiated by [ICRAR](https://www.icrar.org/) and [CSIRO](https://www.csiro.au/) during the SKA pre-construction phase. The work was supported by the Australian Government through the Department of Industry, Innovation and Science under the Round 2 SKA Pre-construction Grants Programme and more recently by the SKA Bridging Grant SKA75656.

The logo image depicted on this page is a licensed reproduction of the aboriginal artistic work *Journey of Self* by Leslie Lee.   

## Questions?

Feel free to open an issue to discuss any questions not covered so far.
