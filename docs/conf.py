# -*- coding: utf-8 -*-
#
# Configuration file for the Sphinx documentation builder.

import os
import subprocess

# Run doxygen if we're in RTD to generate the XML/HTML documentation from C++
read_the_docs_build = os.environ.get('READTHEDOCS', None) == 'True'
if read_the_docs_build:
    subprocess.call('doxygen')

# General project information
project = u'jacal'
copyright = u'2020, YANDA Team'
author = u'YANDA Team'
with open('../VERSION') as f:
    version = f.read()


# General configuration
extensions = ['breathe']
breathe_projects = {
    'jacal': 'doxygen-output/xml'
}
breathe_default_project = 'jacal'
master_doc = 'index'
source_suffix = '.rst'
pygments_style = 'sphinx'
language = None
exclude_patterns = [u'_build', 'Thumbs.db', '.DS_Store']
rst_prolog = '''
.. |dlg| replace:: DALiuGE
.. |egl| replace:: EAGLE
.. |jcl| replace:: JACAL
.. |ys| replace:: Yandasoft
'''


# Format-specific options
html_theme = 'sphinx_rtd_theme'
# html_static_path = ['_static']
html_static_path = []

latex_documents = [
    (master_doc, 'jacal.tex', u'jacal Documentation',
     author, 'manual'),
]
