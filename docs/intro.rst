Introduction
############

|jcl| integrates `Yandasoft <https://yandasoft.readthedocs.io/en/latest/>`_
(previously known as *ASKAPSoft*)
and the execution framework `DALiuGE <https://github.com/ICRAR/daliuge>`_.
A shared library offers a calling convention
supported by |dlg| and internally links and reuses |ys| code.
|jcl| is freely available `in GitLab <https://gitlab.com/ska-telescope/ska-sdp-jacal>`_
under a variation of the open source BSD 3-Clause [License](LICENSE).
The repository contains the following:

* The C/C++ code of the shared library ``libjacal.so`` described above.
* A number of tests running the different components inside |dlg| graphs.
* A standalone utility for library testing independent of |dlg|.


DALIuGE apps
------------

The way |jcl| integrates |ys| into |dlg|
is by wrapping individual pieces of functionality
into |dlg|-compatible applications
that can then be deployed on a |dlg| graph.

|dlg| is an execution framework
where programs are expressed as directed acyclic graphs,
with nodes representing not only the different computations
performed on the data as it flows through the graph,
but also the data itself.
Both types of nodes are termed *drops*.
Computation drops (in |dlg|, *application drops*)
read or receive data from their input data drops,
and write the results into their output data drops.
Data drops on the other hand
are storage-agnostic and host-agnostic,
meaning that regardless of underlying storage and location
application drops can work with their inputs and outputs
in the same way.

Although application drops can be implemented in many ways,
|dlg| offers out-of-the-box support
for certain type of applications.
Among those,
*shared libraries* can be written by users
to implement application drops.
This capability allows reusing code written in C, C++
or other low-level languages
to work as application drops in a |dlg| graph.


Using |ys| in |dlg|
-------------------

Before |jcl|,
the only way to use the |ys| functionality
was to invoke the binaries it generates
(e.g., ``cimager``, ``cbpcalibrator``, etc.);
composition was only possible
by arranging pipelines using shell scripts and similar techniques,
and with data having to touch disk
between each invocation of the binaries.

|jcl| on the other hand implements a shared library
(i.e., ``libjacal.so``)
wrapping different parts of |ys|
as |dlg|-ready application drops.
This makes it possible
to reuse finer-grained pieces of functionality
from the |ys| code base,
and with data not having to be necessarily written to disk
between these steps.
