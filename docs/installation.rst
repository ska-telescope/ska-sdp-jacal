Installation
############

Dependencies
============

|jcl| has two main dependencies (which in turn might require a lot more):
 * The |dlg| execution framework, and
 * The |ys| libraries

Installation for both dependencies is covered below.

----------

Docker Image Installation
=========================

The following installation instructions are recommended for deployment on a
laptop or workstation.

.. See also this `confluence refresher <https://confluence.skatelescope.org/display/SE/YAN-753+Deployment+and+Development+of+DALIUGE+Graphs+with+JACAL+Components+-+A+refresher>`_.

Building the Images
-------------------

|dlg| is available from the ICRAR `github repo
<https://github.com/ICRAR/daliuge>`_. There are three packages:

 * *daliuge-common* -- the base image containing the basic DALiuGE libraries
   and dependencies.
 * *daliuge-translator* -- the DALiuGE translator, built on top of the base
   image. This converts Logical Graphs into Physical Graphs.
 * *daliuge-engine* -- the DALiuGE execution engine, built on top of the base
   image.

.. code-block:: shell

 cd <install dir>
 git clone https://github.com/ICRAR/daliuge.git
 cd daliuge/daliuge-common; ./build_common.sh dev
 cd ../daliuge-translator; ./build_translator.sh dev
 cd ../daliuge-engine; ./build_engine.sh dev

In |jcl|, the standard daliuge-engine is replaced with a |jcl| version that is
based on both the |dlg| base image and the |ys| image.

.. code-block:: shell

 cd <install dir>
 git clone https://gitlab.com/ska-telescope/ska-sdp-jacal
 cd ska-sdp-jacal; ./build_engine.sh jacal

EAGLE is a web-based visual workspace for generating Logical Graphs.

.. code-block:: shell

 cd <install dir>
 git clone https://github.com/ICRAR/EAGLE.git
 cd EAGLE; ./build_eagle.sh dev

Running the Images
------------------

Now that the images have been built, they need to be run. After this they can
be graphed and deployed via |egl|.

.. code-block:: shell

 cd <install dir>
 cd daliuge/daliuge-translator; ./run_translator.sh dev

.. code-block:: shell

 cd <install dir>
 cd ska-sdp-jacal; ./run_engine.sh jacal

.. code-block:: shell

 cd <install dir>
 cd EAGLE; ./run_eagle.sh dev

----------

Bare-metal Installation
=======================

.. note::
  For most use cases the docker installation described above is recommended.

|dlg|
-----

See `DALiuGE documentation <https://daliuge.readthedocs.io/en/latest>`_ for
up-to-date installation and usage information.

|ys|
----

See `Yandasoft documentation <https://yandasoft.readthedocs.io/en/latest/>`_
for up-to-date installation and usage information. Note that |ys| has a list of
dependencies on its own, including casacore, wcslib, cfitsio, fftw, boost,
log4cxx and gsl.

|jcl|
-----

Once |dlg| and |ys| are installed, |jcl| itself can be built. |jcl| uses the
CMake build system, hence the build instructions are those one would expect:

.. code-block:: shell

 cd <install dir>
 git clone https://gitlab.com/ska-telescope/ska-sdp-jacal
 cd ska-sdp-jacal
 mkdir build
 cd build
 cmake ..
 make

This process should generate a ``libjacal.so`` shared library which one can use
within |dlg|'s ``DynlibApp`` components. Stand-alone executables are also
produced under ``test``, which are used for testing the code outside the
context of DALiuGE.

