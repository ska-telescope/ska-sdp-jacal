FROM csirocass/yandasoft:rc-openmpi2

# this is need due to the cmake issues
USER root
RUN rm -f /usr/local/bin/cmake
# This is the daliuge install
RUN apt update && apt install -y g++ git git-lfs cmake python3-pip
USER yanda-user
RUN pip3 install --upgrade pip
RUN pip3 install daliuge
COPY ./ ./
RUN mkdir build && cd build && cmake .. && cmake --build .
USER root
RUN cd ${HOME}/build && make install
USER yanda-user

