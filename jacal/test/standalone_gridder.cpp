//
// Created by Stephen Ord on 29/11/21.
//

#include <fstream>
#include <iostream>
#include <streambuf>
#include <string>
#include <thread>

#include "factory/DaliugeApplicationFactory.h"


namespace askap {

    dlg_app_info *dummy_dlg_app(const std::string &input_file) {

        std::ifstream in(input_file);
        std::ostringstream ss;
        ss << in.rdbuf();
        static thread_local auto contents = ss.str();

        // read method that always returns the contents of input_file
        auto _read = [](char *buf, size_t n) -> size_t {
            if (contents.size() < n) {
                n = contents.size();
            }
            memcpy(buf, contents.c_str(), n);
            return n;
        };

        // write method that writes nothing
        auto _write = [](const char *buf, size_t n) -> size_t {
            return n;
        };

        dlg_input_info *input = (dlg_input_info *) calloc (2,sizeof(dlg_input_info));

        input[0].read = _read;
        input[0].name = new char[7];
        strcpy(input[0].name, "Config");

        //input[1].read = _read;
        //input[1].name = new char[6];
        //strcpy(input[1].name, "Model");



        dlg_output_info *output = new dlg_output_info();
        output->write = _write;
        output->name = new char[6];
        strcpy(output->name, "Normal");
        dlg_app_info *app = new dlg_app_info();
        app->inputs = &input[0];
        app->n_inputs = 1;
        app->outputs = output;
        app->n_outputs = 1;
        app->uid = new char[4];
        app->oid = new char[4];
        app->appname = new char[18];
        strcpy(app->appname, "CalcNE");
        strcpy(app->uid, "uid");
        strcpy(app->oid, "oid");
        return app;
    }

    int run_gridder(const std::string &input_file) {
        const char *arguments[][2] = {{nullptr, nullptr}};
        dlg_app_info *app = dummy_dlg_app(input_file);
        DaliugeApplication::ShPtr j_cal = DaliugeApplicationFactory::make(app);
        j_cal->init((const char ***)arguments);
        return j_cal->run();
    }

    int run(int argc, std::string input1) {
        return run_gridder(input1);
    }

}

int main(int argc, char **argv) {
    if (argc < 2) {
        std::cerr << "Usage: " << argv[0] << " <parset>" << std::endl;
        return 1;
    }

    return askap::run(argc, argv[1]);
}
