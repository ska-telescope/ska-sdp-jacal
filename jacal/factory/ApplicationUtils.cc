/// @file ApplicationUtils.cc

// for logging
#define ASKAP_PACKAGE_NAME "ApplicationUtils"
#include <string>

/// askap namespace
namespace askap {
/// @return version of the package
    std::string getAskapPackageVersion_ApplicationUtils() {
        return std::string("ApplicationUtils");
    }
} // namespace askap

/// The version of the package
#define ASKAP_PACKAGE_VERSION askap::getAskapPackageVersion_ApplicationUtils()

#include <askap/AskapLogging.h>
#include <askap/AskapError.h>

#include <factory/ApplicationUtils.h>

namespace askap {

const std::string ApplicationUtils::nodeName() {

    const int MAX_HOSTNAME_LEN = 1024;
    char name[MAX_HOSTNAME_LEN];
    name[MAX_HOSTNAME_LEN - 1] = '\0';
    const int error = gethostname(name, MAX_HOSTNAME_LEN - 1);
    if (error) {
        ASKAPTHROW(AskapError, "gethostname() returned error: " << error);
    }
    std::string pname(name);
    std::string::size_type idx = pname.find_first_of('.');
    if (idx != std::string::npos) {
        // Extract just the hostname part
        pname = pname.substr(0, idx);
    }
    return pname;

}

void ApplicationUtils::updateLoggerContext(const int procid) {
    ASKAPLOG_REMOVECONTEXT("hostname");
    ASKAPLOG_PUTCONTEXT("hostname", nodeName().c_str());
    ASKAPLOG_REMOVECONTEXT("procid");
    ASKAPLOG_PUTCONTEXT("procid", std::to_string(procid));
}

} // namespace
