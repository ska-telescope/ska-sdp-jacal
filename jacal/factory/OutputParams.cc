/// @file OutputParams.cc

// for logging
#define ASKAP_PACKAGE_NAME "OutputParams"
#include <string>
/// askap namespace
namespace askap {
/// @return version of the package
    std::string getAskapPackageVersion_OutputParams() {
        return std::string("OutputParams");
    }


} // namespace askap

/// The version of the package
#define ASKAP_PACKAGE_VERSION askap::getAskapPackageVersion_OutputParams()

#include <vector>
#include <mutex>



#include <daliuge/DaliugeApplication.h>
#include <factory/OutputParams.h>
#include <factory/NEUtils.h>

// LOFAR ParameterSet
#include <Common/ParameterSet.h>
// LOFAR Blob
#include <Blob/BlobString.h>
#include <Blob/BlobOBufString.h>
#include <Blob/BlobIBufString.h>
#include <Blob/BlobOStream.h>
#include <Blob/BlobIStream.h>

// Base ASKAP
#include <askap/AskapLogging.h>
#include <askap/AskapError.h>

// Data accessors
#include <dataaccess/TableConstDataSource.h>
#include <dataaccess/IConstDataIterator.h>
#include <dataaccess/IDataConverter.h>
#include <dataaccess/IDataSelector.h>
#include <dataaccess/IDataIterator.h>
#include <dataaccess/SharedIter.h>

// params helpers
#include <measurementequation/SynthesisParamsHelper.h>
#include <measurementequation/ImageParamsHelper.h>
#include <measurementequation/ImageFFTEquation.h>
#include <parallel/GroupVisAggregator.h>


#include <gridding/IVisGridder.h>
#include <gridding/VisGridderFactory.h>

// Solver stuff

#include <measurementequation/ImageSolverFactory.h>



#include <scimath/fitting/Params.h>
#include <scimath/fitting/Solver.h>

#include <string.h>
#include <sys/time.h>




namespace askap {



    OutputParams::OutputParams(dlg_app_info *raw_app) :
        DaliugeApplication(raw_app)
    {
    }


    OutputParams::~OutputParams() {
    }

    DaliugeApplication::ShPtr OutputParams::createDaliugeApplication(dlg_app_info *raw_app)
    {
        return OutputParams::ShPtr(new OutputParams(raw_app));
    }

    int OutputParams::init(const char ***arguments) {



        while (1) {

            const char **param = *arguments;

            // Sentinel
            if (param == NULL) {
                break;
            }
            // if (strcmp(param[0], "config") == 0) {
            //    config_key = strdup(param[1]);
            // }

            arguments++;
        }

        return 0;
    }

    int OutputParams::run() {

#ifndef ASKAP_PATCHED
        static std::mutex safety;
        std::lock_guard<std::mutex> guard(safety);
#endif // ASKAP_PATCHED

        ASKAP_LOGGER(logger, ".OutputParams.run");

        // Lets get the key-value-parset
        // lets open the input and read it
        char buf[64*1024];
        size_t n_read = input("Config").read(buf, 64*1024);

        LOFAR::ParameterSet parset(true);
        parset.adoptBuffer(buf);

        try {
            NEUtils::finaliseParameters(parset);
        }
        catch (std::runtime_error)
        {
            return -1;
        }
        this->itsParset = parset.makeSubset("Cimager.");

        synthesis::SynthesisParamsHelper::setUpImageHandler(itsParset);

        // Set checks up-front, so we can decide what to output if any Params appear on multiple input ports

        // Are there Model Params to input?
        // Note that has_input() and input() search for this string as a substring.
        // Add regex characters to enforce full-string match
        const string model_string = "^Model$";
        const auto has_model = has_input(model_string);

        // Are there Restored Model Params to input?
        const string restored_model_string = "Restored Model";
        const auto has_restored_model = has_input(restored_model_string);

        if (has_model) {
            ASKAPLOG_INFO_STR(logger, "Receiving "<<model_string<<" Params");
            // reset the params and load in new input
            this->itsModel.reset(new scimath::Params(true));
            NEUtils::receiveParams(itsModel, input(model_string));
            vector<string> images=itsModel->names();
            for (vector<string>::const_iterator it=images.begin(); it !=images.end(); it++) {
                if (it->find("image") == 0) {
                    ASKAPLOG_INFO_STR(logger, model_string<<" contains "<<*it);
                    const synthesis::ImageParamsHelper iph(*it);
                    synthesis::SynthesisParamsHelper::saveImageParameter(*itsModel, *it, *it);
                }
                if (it->find("residual") == 0) {
                    ASKAPLOG_INFO_STR(logger, model_string<<" contains "<<*it);
                    const synthesis::ImageParamsHelper iph(*it);
                    synthesis::SynthesisParamsHelper::saveImageParameter(*itsModel, *it, *it+".dirty");
                }
                if (it->find("weights") == 0) {
                    ASKAPLOG_INFO_STR(logger, model_string<<" contains "<<*it);
                    const synthesis::ImageParamsHelper iph(*it);
                    synthesis::SynthesisParamsHelper::saveImageParameter(*itsModel, *it, *it);
                }
                // Write the following only if !has_restored_model?
                // psf?
                // psf.image?
                // weights?
                // sensitivity?
                // mask?
            }
        }

        if (has_restored_model) {
            ASKAPLOG_INFO_STR(logger, "Receiving "<<restored_model_string<<" Params");
            // reset the params and load in new input
            this->itsModel.reset(new scimath::Params(true));
            NEUtils::receiveParams(itsModel, input(restored_model_string));
            vector<string> images=itsModel->names();
            for (vector<string>::const_iterator it=images.begin(); it !=images.end(); it++) {
                if (it->find("image") == 0) {
                    ASKAPLOG_INFO_STR(logger, restored_model_string<<" contains "<<*it);
                    const synthesis::ImageParamsHelper iph(*it);
                    synthesis::SynthesisParamsHelper::saveImageParameter(*itsModel, *it, *it+".restored");
                }
                if (it->find("residual") == 0) {
                    ASKAPLOG_INFO_STR(logger, restored_model_string<<" contains "<<*it);
                    const synthesis::ImageParamsHelper iph(*it);
                    synthesis::SynthesisParamsHelper::saveImageParameter(*itsModel, *it, *it);
                }
                // Write the following only if !has_model?
                // psf?
                // psf.image?
                // weights?
                // sensitivity?
                // mask?
            }
        }

        return 0;
    }


    void OutputParams::data_written(const char *uid, const char *data, size_t n) {
        dlg_app_running();
    }

    void OutputParams::drop_completed(const char *uid, drop_status status) {
        dlg_app_done(APP_FINISHED);
    }


} // namespace
