/// @file OnDemandBufferDataAccessor.h
///
/// @brief an adapter to most methods of accessors::IConstDataAccessor with
/// buffering
/// @details This class, based on accessors::OnDemandBufferDataAccessor,
/// returns the existing read-only visibility cube until a non-const reference
/// is requested (rwVisibility). Then the read-only visibilities are copied to
/// the internal buffer and the reference to this buffer is passed for all
/// later calls to read-write and read-only methods until either the shape
/// changes or discardCache method is called.
///
/// @author Max Voronkov <maxim.voronkov@csiro.au>

#ifndef ASKAP_FACTORY_ON_DEMAND_BUFFER_DATA_ACCESSOR_H
#define ASKAP_FACTORY_ON_DEMAND_BUFFER_DATA_ACCESSOR_H

// Base Accessors
#include <dataaccess/MetaDataAccessor.h>
#include <dataaccess/IDataAccessor.h>

// boost includes
#include <boost/noncopyable.hpp>
#ifdef _OPENMP
#include <boost/thread/shared_mutex.hpp>
#endif

namespace askap {

/// @brief an adapter to most methods of IConstDataAccessor with buffering
/// @ingroup dataaccess_hlp
class OnDemandBufferDataAccessor : virtual public accessors::MetaDataAccessor,
                                   virtual public accessors::IDataAccessor,
                                   public boost::noncopyable
{
public:
  /// construct an object linked with the given const accessor
  /// @param[in] acc a reference to the associated accessor
  explicit OnDemandBufferDataAccessor(const accessors::IConstDataAccessor &acc);
  
  /// Read-only visibilities (a cube is nRow x nChannel x nPol; 
  /// each element is a complex visibility)
  ///
  /// @return a reference to nRow x nChannel x nPol cube, containing
  /// all visibility data
  ///
  virtual const casacore::Cube<casacore::Complex>& visibility() const;

	
  /// Read-write access to visibilities (a cube is nRow x nChannel x nPol;
  /// each element is a complex visibility)
  ///
  /// @return a reference to nRow x nChannel x nPol cube, containing
  /// all visibility data
  ///
  virtual casacore::Cube<casacore::Complex>& rwVisibility();
  
  /// @brief discard the content of the cache
  /// @details A call to this method would switch the accessor to the pristine state
  /// it had straight after construction. A new call to rwVisibility would be required 
  /// to decouple from the read-only accessor 
  void discardCache();
  
  /// @brief check whether this class is decoupled from the original accessor
  /// @details The first write request triggers creation of a buffer, which is used 
  /// for all future read and write operations, until discardCache is called or the
  /// visibility cube changes shape. This method allows to check the state of this class.
  /// It returns true if all reads and writes are coming from the buffer, rather than the
  /// original accessor. 
  /// @return true of this class uses buffer
  bool isDecoupled() const throw() { return itsUseBuffer; }
  
private:
  /// @brief a helper method to check whether the buffer has a correct size
  /// @details The wrong size means that the iterator has advanced and this
  /// accessor has to be coupled back to the read-only accessor which has been given at the 
  /// construction. If a wrong size is detected, itsUseBuffer flag is reset.
  void checkBufferSize() const;
  
  /// @brief is buffer used?
  /// @details true, if accessor is coupled 
  mutable bool itsUseBuffer;
  
  /// @brief actual buffer
  /// @details A zero shape means that this class is coupled to read-only accessor, rather than
  /// this buffer.
  mutable casacore::Cube<casacore::Complex> itsBuffer;

  #ifdef _OPENMP
  /// @brief synchronisation object
  mutable boost::shared_mutex itsMutex;
  #endif
};

} // namespace askap


#endif // #ifndef ASKAP_FACTORY_ON_DEMAND_BUFFER_DATA_ACCESSOR_H
