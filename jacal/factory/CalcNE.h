/// @file CalcNE.h
///
/// @brief Calculate imaging Normal Equations (vis predict followed by invert)
/// @details This class encorporates all of the tasks needed to form imaging
/// Normal Equations: read from a measurement set; degrid model visibilities;
/// subtract model visibilities; grid residual visibilities and FFT the grid
///
/// @author Tim Cornwell <tim.cornwell@csiro.au>
/// @author Stephen Ord <stephen.ord@csiro.au>
/// @author Daniel Mitchell <tim.cornwell@csiro.au>

#ifndef ASKAP_FACTORY_CALCNE_H
#define ASKAP_FACTORY_CALCNE_H

#include "rename.h"
#include <daliuge/DaliugeApplication.h>

#include <factory/gridders/VisData.h>

#include <casacore/casa/Quanta/MVDirection.h>

#include <boost/shared_ptr.hpp>

// LOFAR ParameterSet
#include <Common/ParameterSet.h>

// Yandasoft includes
#include <scimath/fitting/Params.h>

namespace askap {

    /*!
    * \brief CalcNE
    * \brief Calculates the Normal Equations
    * \details This class encorporates all of the tasks needed to form imaging
    *  Normal Equations: read from a measurement set; degrid model visibilities;
    *  subtract model visibilities; grid residual visibilities and FFT the grid
    * \par EAGLE_START
    * \param category DynlibApp
    * \param[in] param/libpath Library Path/"%JACAL_SO%"/String/readonly/
    *     \~English The path to the JACAL librarc
    * \param[in] param/Arg01 Arg01/name=CalcNE/String/readonly/
    *     \~English
    * \param[in] port/Config Config/LOFAR::ParameterSet/
    *     \~English ParameterSet descriptor for the image solver
    * \param[in] port/Model Model/scimath::Params/
    *     \~English Params of solved normal equations
    * \param[out] port/Normal Normal/scimath::ImagingNormalEquations/
    *     \~English ImagingNormalEquations to solve
    * \par EAGLE_END
    */
    class CalcNE : public DaliugeApplication
    {
    public:

        typedef boost::shared_ptr<CalcNE> ShPtr;

        CalcNE(dlg_app_info *raw_app);

        static inline std::string ApplicationName() { return "CalcNE";}

        virtual ~CalcNE();

        static DaliugeApplication::ShPtr createDaliugeApplication(dlg_app_info *raw_app);

        virtual int init(const char ***arguments);

        virtual int run();

        virtual void data_written(const char *uid, const char *data, size_t n);

        virtual void drop_completed(const char *uid, drop_status status);


        private:

            //! @brief The model
            //! @details contains a set of parameters - essentially the solution of the NE
            askap::scimath::Params::ShPtr itsModel;

            //! @brief Parameter set
            //! @details key value list of configuration options
            LOFAR::ParameterSet itsParset;

            // Its tangent point
            std::vector<casacore::MVDirection> itsTangent; ///< the tangent point of the current grid

            int itsRank;

            // cached VisData objects
            static std::vector<VisData::ShPtr> itsVisData;

            // utility to build an Imaging Normal Equation from a parset
            // void buildNE();

            // these are the steps required by buildNE

    };

} // namespace askap


#endif //

