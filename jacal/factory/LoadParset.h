/// @file LoadParset.h

#ifndef ASKAP_FACTORY_LOADPARSET_H
#define ASKAP_FACTORY_LOADPARSET_H

#include "rename.h"

#include <daliuge/DaliugeApplication.h>

#include <boost/shared_ptr.hpp>
#include <boost/optional.hpp>

namespace askap {
    /*!
    * \brief LoadParset
    * \brief Load a LOFAR Parameter Set in the DaliugeApplication Framework
    * \details Loads a configuration from a file drop and generates a LOFAR::ParameterSet
    * The first ASKAP example in the Daliuge framework that actually
    * performs an ASKAP related task.
    * We load a parset into memory from either a file or another Daliuge drop_status
    * \par EAGLE_START
    * \param category DynlibApp
    * \param[in] param/libpath Library Path/"%JACAL_SO%"/String/readonly/
    *     \~English The path to the JACAL library
    * \param[in] param/Arg01 Arg01/name=LoadParset/String/readonly/
    *     \~English
    * \param[in] port/Config Config/LOFAR::ParameterSet/
    *     \~English ParameterSet descriptor for the image solver
    * \param[out] port/Config Config/LOFAR::ParameterSet/
    *     \~English
    * \par EAGLE_END
    */
    class LoadParset : public DaliugeApplication
    {
    public:

        typedef boost::shared_ptr<LoadParset> ShPtr;

        LoadParset(dlg_app_info *raw_app);

        static inline std::string ApplicationName() { return "LoadParset";}

        virtual ~LoadParset();

        static DaliugeApplication::ShPtr createDaliugeApplication(dlg_app_info *raw_app);

        virtual int init(const char ***arguments);

        virtual int run();

        virtual void data_written(const char *uid, const char *data, size_t n);

        virtual void drop_completed(const char *uid, drop_status status);

        private:

        // variables obtained if attached as the input application of a DALiuGE scatter drop
        boost::optional<int> its_num_of_copies;
        boost::optional<std::string> its_scatter_axis;

    };

} // namespace askap


#endif //
