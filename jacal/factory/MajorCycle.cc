/// @file MajorCycle.cc

/// for logging
#define ASKAP_PACKAGE_NAME "MajorCycle"
#include <string>
/// askap namespace
namespace askap {
/// @return version of the package
    std::string getAskapPackageVersion_MajorCycle() {
        return std::string("MajorCycle; ASKAPsoft==Unknown");

    }
}
/// The version of the package
#define ASKAP_PACKAGE_VERSION askap::getAskapPackageVersion_MajorCycle()

#include <vector>
#include <mutex>

// Local includes
#include <daliuge/DaliugeApplication.h>
#include <factory/MajorCycle.h>
#include <factory/NEUtils.h>

// Yandasoft Data Source and Iterator classes (common to all data access methods that use Yandasoft DataAccessors)
#include <dataaccess/TableDataSource.h>
#include <dataaccess/IDataIterator.h>
#include <dataaccess/SharedIter.h>
#include <dataaccess/IDataConverter.h>
#include <dataaccess/IDataSelector.h>

// Yandasoft DataAccessors, DataIterators and Gridders
#include <measurementequation/ImageFFTEquation.h>
#include <gridding/IVisGridder.h>
#include <gridding/VisGridderFactory.h>

// Yandasoft DataAccessors with local GriddingInfos and Gridders
#include <factory/ImageEquation.h>
#include <factory/gridders/IVisGridder.h>
#include <factory/gridders/VisGridderFactory.h>

// LOFAR ParameterSet
#include <Common/ParameterSet.h>
// LOFAR Blob
#include <Blob/BlobString.h>
#include <Blob/BlobOBufString.h>
#include <Blob/BlobIBufString.h>
#include <Blob/BlobOStream.h>
#include <Blob/BlobIStream.h>

// Base ASKAP
#include <askap/AskapLogging.h>
#include <askap/AskapError.h>
#include <askap/StatReporter.h>

// params helpers
#include <measurementequation/SynthesisParamsHelper.h>
#include <measurementequation/ImageParamsHelper.h>
#include <measurementequation/ImageFFTEquation.h>
#include <parallel/GroupVisAggregator.h>

#include <scimath/fitting/Params.h>

#include <string.h>
#include <sys/time.h>

namespace askap {

    // disabling this cache, as it doesn't work with parallel facets
    //std::vector<VisData::ShPtr> MajorCycle::itsVisData;

    MajorCycle::MajorCycle(dlg_app_info *raw_app) :
        DaliugeApplication(raw_app)
    {
        this->itsModel.reset(new scimath::Params(true));
        this->itsRank = NEUtils::getRank(raw_app->uid);
        if (this->itsRank < 0) {
          this->itsRank = 0;
        }
    }

    MajorCycle::~MajorCycle() {
    }

    DaliugeApplication::ShPtr MajorCycle::createDaliugeApplication(dlg_app_info *raw_app)
    {
        return MajorCycle::ShPtr(new MajorCycle(raw_app));
    }

    int MajorCycle::init(const char ***arguments) {

        while (1) {

            const char **param = *arguments;

            // Sentinel
            if (param == NULL) {
                break;
            }
            // any params I might need go here

            arguments++;
        }

        return 0;
    }

    int MajorCycle::run() {


#ifndef ASKAP_PATCHED
        static std::mutex safety;
#endif // ASKAP_PATCHED

        ASKAP_LOGGER(logger, ".MajorCycle.run");

        askap::StatReporter stats;

        // Lets get the key-value-parset
        char buf[64*1024];

        if (has_input("Config")) {
            size_t n_read = input("Config").read(buf, 64*1024);
            if (n_read == 0) {
                ASKAPLOG_WARN_STR(logger, "Nothing read from input Config");
                return -1;
            }
            else if (n_read == 64*1024) {
                n_read--;
            }
            buf[n_read] = 0;
        } else {
            ASKAPLOG_WARN_STR(logger, "No input Config");
            return -1;
        }

        LOFAR::ParameterSet parset(true);
        parset.adoptBuffer(buf);

        // we need to fill the local parset with parameters that maybe missing
        try {
            NEUtils::finaliseParameters(parset, this->itsRank);
        }
        catch (std::runtime_error)
        {
            return -1;
        }
        this->itsParset = parset.makeSubset("Cimager.");

        // after finaliseParameters the following should be set and should be integers
        const std::vector<int> ChannelsParam = parset.getIntVector("Cimager.Channels");

        const string colName = this->itsParset.getString("datacolumn", "DATA");
        vector<std::string> ms = NEUtils::getDatasets(this->itsParset);
        ASKAPCHECK(ms.size()==1, "Only a single measurement set can be processed at this stage. You have "<<ms);

        // Do we have a model?
        // If so, read it in.
        // If not, there are two reasons:
        //  - nothing is connected to the input port, in which case set up new image grids
        //  - something is connected but it's empty. Assume it's the first iteration and carry on?
        auto has_model = has_input("Model");
        if (has_model) {
            this->itsModel.reset(new scimath::Params(true));
            try {
                NEUtils::receiveParams(itsModel, input("Model"));
                ASKAPLOG_INFO_STR(logger, "Received model images");
            }
            catch (std::runtime_error) {
                ASKAPLOG_INFO_STR(logger, "Failed to receive model images");
                has_model = false;
            }
        }
        // If there is no input image or nothing coming from the input, create empty model images
        if (!has_model) {
            ASKAPLOG_INFO_STR(logger, "Initializing empty model images");
            // Create the specified images from the definition in the parameter set. We can solve
            // for any number of images at once (but you may/will run out of memory!)
            askap::synthesis::SynthesisParamsHelper::setUpImages(itsModel, this->itsParset.makeSubset("Images."));
        }
        ASKAPLOG_INFO_STR(logger, "Current model held by the drop: "<<*itsModel);

        // NE
        askap::scimath::ImagingNormalEquations::ShPtr itsNe;
        itsNe = askap::scimath::ImagingNormalEquations::ShPtr(new askap::scimath::ImagingNormalEquations(*itsModel));

        // I cant make the gridder smart funciton a member funtion as I cannot instantiate it until I have a parset.

        ASKAPLOG_INFO_STR(logger, "Processing " << ms[0]);

        accessors::TableDataSource ds(ms[0], accessors::TableDataSource::DEFAULT, colName);

        // this is the data selector for the measurement set
        accessors::IDataSelectorPtr sel = ds.createSelector();

        sel->chooseCrossCorrelations();
        sel->chooseChannels(ChannelsParam[0], ChannelsParam[1]);

        ASKAPLOG_INFO_STR(logger,"Selecting "<<ChannelsParam[0]<<" channels starting at "<<ChannelsParam[1]);

        // FIXME: Use time interval and perhaps beam?

        accessors::IDataConverterPtr conv = ds.createConverter();
        conv->setFrequencyFrame(casacore::MFrequency::Ref(casacore::MFrequency::TOPO), "Hz");
        conv->setDirectionFrame(casacore::MDirection::Ref(casacore::MDirection::J2000));
        conv->setEpochFrame();

        accessors::IDataSharedIter it = ds.createIterator(sel, conv);

        ASKAPLOG_INFO_STR(logger,"Not applying calibration");

        // get the data access type.
        //  - "yandasoft"      yandasoft base-accessor data access and iteration, as well as yandasoft gridders
        //  - "datapartitions" jacal gridding-data partitions and gridders (using yandasoft DataAccessors)
        //  - "???"            jacal gridding-data partitions and gridders (using daliuge apache plasma classes)
        // related parset parameters
        //  - Cimager.gridder.partitiontype = time or wsort
        const string dataAccess = this->itsParset.getString("gridder.dataaccess", "yandasoft");
        ASKAPLOG_INFO_STR(logger,"Using gridder dataaccess type \""<<dataAccess<<"\"");

        if (dataAccess == "yandasoft") {
            if ( this->itsParset.isDefined("gridder.partitiontype") ) {
                ASKAPLOG_WARN_STR(logger," - gridder partitiontype ignored with dataaccess = \""<<dataAccess<<"\"");
            }

            // build a gridder
            askap::synthesis::IVisGridder::ShPtr
                itsGridder = askap::synthesis::VisGridderFactory::make(this->itsParset);
         
            // Define an imaging equation with an empty model (or pick one up from the parset)
            ASKAPLOG_INFO_STR(logger,"building FFT/measurement equation");
            boost::shared_ptr<askap::synthesis::ImageFFTEquation>
                fftEquation(new askap::synthesis::ImageFFTEquation (*itsModel, it, itsGridder));
            ASKAPDEBUGASSERT(fftEquation);
            fftEquation->useAlternativePSF(itsParset);
         
            // Leave this set to an empty pointer - no aggregation across groups for this
            // FIXME: This will be needed if we ever do distributed Taylor terms in the way they are done in
            //        yandasoft, with different terms on different ranks. However I believe the CDR plan for SKA
            //        is have a single gridder per frequency, and apply the MFS weighting during multiple image
            //        reductions. In that case all Taylor term degriddings operations for a given channel may
            //        be done on the same node.
            //fftEquation->setVisUpdateObject(boost::shared_ptr<askap::synthesis::GroupVisAggregator>());

#ifndef ASKAP_PATCHED
                std::lock_guard<std::mutex> guard(safety);
#endif // ASKAP_PATCHED

            // Generate the imaging normal equations (grid and FFT)
            ASKAPLOG_INFO_STR(logger,"calculating Imaging Normal Equations");
            fftEquation->calcImagingEquations(*itsNe);

        } else if (dataAccess == "datapartitions") {
            if ( this->itsParset.isDefined("gridder.partitiontype") ) {
                ASKAPLOG_INFO_STR(logger," - gridder partitiontype: "<<
                    this->itsParset.getString("gridder.partitiontype"));
            }

            // build a gridder
            IVisGridder::ShPtr gridder = VisGridderFactory::make(this->itsParset);

            // Define an imaging equation with an empty model (or pick one up from the parset)
            ASKAPLOG_INFO_STR(logger,"Building FFT/measurement equation");
            boost::shared_ptr<ImageEquation> imgEq(new ImageEquation (*itsModel, it, gridder, itsParset));
            ASKAPDEBUGASSERT(imgEq);

            // Leave this set to an empty pointer - no aggregation across groups for this
            // FIXME: This will be needed if we ever do distributed Taylor terms in the way they are done in
            //        yandasoft, with different terms on different ranks. However I believe the CDR plan for SKA
            //        is have a single gridder per frequency, and apply the MFS weighting during multiple image
            //        reductions. In that case all Taylor term degriddings operations for a given channel may
            //        be done on the same node.
            //imgEq->setVisUpdateObject(boost::shared_ptr<askap::synthesis::GroupVisAggregator>());

#ifndef ASKAP_PATCHED
                std::lock_guard<std::mutex> guard(safety);
#endif // ASKAP_PATCHED

            // Eventually the following static vector cache will need to be replaced by VisData IO ports to
            // use the full functionality of Daliuge and avoid preventing certain deployment patterns from working.
            // This should just be a matter of having a single, non-static VisData::ShPtr that is connected to
            // the ports and fed to imgEq (see code below).
            //  - Just need to sort out blob strings for the GD and GDP classes. 
            // For now, use a static vector cache with one VisData::ShPtr for each parallel process.
            //  - In MPI-like mode, only one of the pointers would be set for a given process.
            //  - in a Daliuge-like mode, the pointers for all processes on a given node would be set.
            if (itsVisData.size() == 0) {
                // with the lock above, only one of the parallel copies should get here
                // @todo get maxNproc (or actual Nproc) properly. From scatter drop or parset?
                const int maxNproc = NEUtils::getNChan(this->itsParset);
                ASKAPLOG_INFO_STR(logger,"Resizing the VisData cache to "<<maxNproc);
                itsVisData.resize(maxNproc);
            }
            // if this copy's cache has not been filled with data partitions, do it now
            if (itsVisData[this->itsRank] == nullptr) {
                ASKAPLOG_INFO_STR(logger,"filling VisData cache for channel "<<this->itsRank);
                itsVisData[this->itsRank] = imgEq->partitionData();
            }
            // set the current VisData object as the one to use for gridding
            ASKAPLOG_INFO_STR(logger,"Locking in the VisData cache for channel "<<this->itsRank);
            imgEq->setDataPartitions(itsVisData[this->itsRank]);

            // set pointer to existing data partitions or generate new ones
            // @todo set up blob streams for VisData and GriddingInfo classes to enable this
            // @todo or could just set up a blob stream for itsPartition caches...
            //VisData::ShPtr gd;
            //auto has_data = has_input("VisData");
            //if (has_data) {
            //    ASKAPLOG_WARN_STR(logger, "VisData data drops not yet supported. Ignoring input.");
            //    has_data = false;
            //}
            //// If there is no input VisData, generate it now 
            ////  - only really need to do it here if sending to an output port
            //if (!has_data) {
            //    ASKAPLOG_INFO_STR(logger, "generating VisData partitions");
            //    gd = imgEq->partitionData();
            //}
            //imgEq->setDataPartitions(gd);

            // Generate the imaging normal equations (grid and FFT)
            ASKAPLOG_INFO_STR(logger,"Calculating Imaging Normal Equations");
            imgEq->calcImagingEquations(*itsNe);

        } else {
            ASKAPTHROW(AskapError, "gridder.dataaccess type \""<<dataAccess<<"\" not supported.");
        }

#ifndef ASKAP_PATCHED
            std::lock_guard<std::mutex> guard(safety);
#endif // ASKAP_PATCHED

        // Send the resulting normal equation images to the output port
        NEUtils::sendNE(itsNe, output("Normal"));

        // I am going to assume a single Ne output - even though I am not
        // merging in the above loop - I should tho.

        // This is just to test whether this works at all.

        stats.logSummary();

        return 0;
    }

    void MajorCycle::data_written(const char *uid, const char *data, size_t n) {
        dlg_app_running();
    }

    void MajorCycle::drop_completed(const char *uid, drop_status status) {
        dlg_app_done(APP_FINISHED);
    }

} // namespace
