/// @file CalcNE.cc
///
/// @details Implements a test method that uses the contents of the parset
/// to load in a measurement set and print a summary of its contents
///
/// @author Tim Cornwell <tim.cornwell@csiro.au>
/// @author Stephen Ord <stephen.ord@csiro.au>
/// @author Daniel Mitchell <daniel.mitchell@csiro.au>

// for logging
#define ASKAP_PACKAGE_NAME "CalcNE"
#include <string>
/// askap namespace
namespace askap {
/// @return version of the package
    std::string getAskapPackageVersion_CalcNE() {
        return std::string("CalcNE; ASKAPsoft==Unknown");

    }
}
/// The version of the package
#define ASKAP_PACKAGE_VERSION askap::getAskapPackageVersion_CalcNE()

#include <vector>
#include <mutex>

// Local includes
#include <daliuge/DaliugeApplication.h>
#include <factory/CalcNE.h>
#include <factory/NEUtils.h>

// Yandasoft Data Source and Iterator classes (common to all data access methods that use Yandasoft DataAccessors)
#include <dataaccess/TableDataSource.h>
#include <dataaccess/IDataIterator.h>
#include <dataaccess/SharedIter.h>
#include <dataaccess/IDataConverter.h>
#include <dataaccess/IDataSelector.h>

// Yandasoft DataAccessors, DataIterators and Gridders
#include <measurementequation/ImageFFTEquation.h>
#include <gridding/IVisGridder.h>
#include <gridding/VisGridderFactory.h>

// Yandasoft DataAccessors with local GriddingInfo and Gridders
#include <factory/ImageEquation.h>
#include <factory/gridders/IVisGridder.h>
#include <factory/gridders/VisGridderFactory.h>

// LOFAR ParameterSet
#include <Common/ParameterSet.h>
// LOFAR Blob
#include <Blob/BlobString.h>
#include <Blob/BlobOBufString.h>
#include <Blob/BlobIBufString.h>
#include <Blob/BlobOStream.h>
#include <Blob/BlobIStream.h>

// Base ASKAP
#include <askap/AskapLogging.h>
#include <askap/AskapError.h>
#include <askap/StatReporter.h>

// params helpers
#include <measurementequation/SynthesisParamsHelper.h>
#include <measurementequation/ImageParamsHelper.h>
#include <parallel/GroupVisAggregator.h>

#include <scimath/fitting/Params.h>

#include <string.h>
#include <sys/time.h>

namespace askap {

    std::vector<VisData::ShPtr> CalcNE::itsVisData;

    CalcNE::CalcNE(dlg_app_info *raw_app) :
        DaliugeApplication(raw_app)
    {
        this->itsModel.reset(new scimath::Params(true));
        this->itsRank = NEUtils::getRank(raw_app->uid);
        if (this->itsRank < 0) {
            this->itsRank = 0;
        }
    }

    CalcNE::~CalcNE() {
    }

    DaliugeApplication::ShPtr CalcNE::createDaliugeApplication(dlg_app_info *raw_app)
    {
        return CalcNE::ShPtr(new CalcNE(raw_app));
    }

    int CalcNE::init(const char ***arguments) {
        ASKAP_LOGGER(logger, ".CalcNE.init");

        while (1) {

            const char **param = *arguments;

            // Sentinel
            if (param == NULL) {
                break;
            }
            // any params I might need go here

            arguments++;
        }

        return 0;
    }

    int CalcNE::run() {

#ifndef ASKAP_PATCHED
        static std::mutex safety;
#endif

        ASKAP_LOGGER(logger, ".CalcNE.run");

        askap::StatReporter stats;

        // Lets get the key-value-parset
        char buf[64*1024];

        if (has_input("Config")) {
            size_t n_read = input("Config").read(buf, 64*1024);
            if (n_read == 0) {
                ASKAPLOG_WARN_STR(logger, "Nothing read from input Config");
                return -1;
            }
        } else {
            ASKAPLOG_WARN_STR(logger, "No input Config");
            return -1;
        }

        LOFAR::ParameterSet parset(true);
        parset.adoptBuffer(buf);

        // we need to fill the local parset with parameters that maybe missing
        try {
#ifndef ASKAP_PATCHED
            std::lock_guard<std::mutex> guard(safety);
#endif
            NEUtils::finaliseParameters(parset, this->itsRank);
        }
        catch (std::runtime_error)
        {
            return -1;
        }
        this->itsParset = parset.makeSubset("Cimager.");

        // after finaliseParameters the following should be set and should be integers
        const std::vector<int> ChannelsParam = parset.getIntVector("Cimager.Channels");

        const string colName = this->itsParset.getString("datacolumn", "DATA");
        vector<std::string> ms = NEUtils::getDatasets(this->itsParset);
        ASKAPCHECK(ms.size()==1, "Only a single measurement set can be processed at this stage. You have "<<ms);

        // Do we have a model?
        // often the first cycle will start a new model from scratch and require no input
        // FIXME: This seems to be looking at the Config port. But one of the drops was Config drop...
        if (has_input("Model")) {
            ASKAPLOG_INFO_STR(logger, "Receiving model images");
            NEUtils::receiveParams(itsModel, input("Model"));
        }
        else {

#ifndef ASKAP_PATCHED
            std::lock_guard<std::mutex> guard(safety);
#endif
            ASKAPLOG_INFO_STR(logger, "Initializing the model images");

            // Create the specified images from the definition in the
            // parameter set. We can solve for any number of images
            // at once (but you may/will run out of memory!)

            askap::synthesis::SynthesisParamsHelper::setUpImages(itsModel, this->itsParset.makeSubset("Images."));
        }

        ASKAPLOG_INFO_STR(logger, "Current model held by the drop: "<<*itsModel);

        {
#ifndef ASKAP_PATCHED
            std::lock_guard<std::mutex> guard(safety);
#endif

            // initialise a normal equation
            askap::scimath::ImagingNormalEquations::ShPtr ne;
            ne = askap::scimath::ImagingNormalEquations::ShPtr(new askap::scimath::ImagingNormalEquations(*itsModel));

            // set up base-accessor Data Source and Iterator classes
            ASKAPCHECK(ms.size()==1, "Only a single measurement set can be processed at this stage.");
            ASKAPLOG_INFO_STR(logger, "Processing " << ms[0]);

            accessors::TableDataSource ds(ms[0], accessors::TableDataSource::DEFAULT, colName);

            // this is the data selector for the measurement set
            accessors::IDataSelectorPtr sel = ds.createSelector();

            sel->chooseCrossCorrelations();
            sel->chooseChannels(ChannelsParam[0], ChannelsParam[1]);

            accessors::IDataConverterPtr conv = ds.createConverter();
            conv->setFrequencyFrame(casacore::MFrequency::Ref(casacore::MFrequency::TOPO), "Hz");
            conv->setDirectionFrame(casacore::MDirection::Ref(casacore::MDirection::J2000));
            conv->setEpochFrame();

            accessors::IDataSharedIter it = ds.createIterator(sel, conv);

            ASKAPLOG_INFO_STR(logger,"Not applying calibration");

            // get the data access type.
            //  - "yandasoft"      yandasoft base-accessor data access and iteration, as well as yandasoft gridders
            //  - "datapartitions" jacal gridding-data partitions and gridders (using yandasoft DataAccessors)
            //  - "???"            jacal gridding-data partitions and gridders (using daliuge apache plasma classes)
            // related parset parameters
            //  - Cimager.gridder.partitiontype = time or wsort
            const string dataAccess = this->itsParset.getString("gridder.dataaccess", "datapartitions");
            ASKAPLOG_INFO_STR(logger,"Using gridder dataaccess type \""<<dataAccess<<"\"");

            if (dataAccess == "yandasoft") {
                if ( this->itsParset.isDefined("gridder.partitiontype") ) {
                    ASKAPLOG_WARN_STR(logger," - gridder partitiontype ignored with dataaccess = \""<<dataAccess<<"\"");
                }

                // build a gridder
                askap::synthesis::IVisGridder::ShPtr
                    gridder = askap::synthesis::VisGridderFactory::make(this->itsParset);

                // Define an imaging equation with an empty model (or pick one up from the parset)
                ASKAPLOG_INFO_STR(logger,"Building FFT/measurement equation");
                boost::shared_ptr<askap::synthesis::ImageFFTEquation>
                    fftEquation(new askap::synthesis::ImageFFTEquation (*itsModel, it, gridder));
                ASKAPDEBUGASSERT(fftEquation);
                fftEquation->useAlternativePSF(itsParset);

                // Leave this set to an empty pointer - no aggregation across groups for this
                // FIXME: This will be needed if we ever do distributed Taylor terms in the way they are done in
                //        yandasoft, with different terms on different ranks. However I believe the CDR plan for SKA
                //        is have a single gridder per frequency, and apply the MFS weighting during multiple image
                //        reductions. In that case all Taylor term degriddings operations for a given channel may
                //        be done on the same node.
                //fftEquation->setVisUpdateObject(boost::shared_ptr<askap::synthesis::GroupVisAggregator>());
             
                // Generate the imaging normal equations (grid and FFT)
                ASKAPLOG_INFO_STR(logger,"calculating Imaging Normal Equations");
                fftEquation->calcImagingEquations(*ne);

            } else if (dataAccess == "datapartitions") {
                if ( this->itsParset.isDefined("gridder.partitiontype") ) {
                    ASKAPLOG_INFO_STR(logger," - gridder partitiontype: "<<
                        this->itsParset.getString("gridder.partitiontype"));
                }

                // build a gridder
                IVisGridder::ShPtr gridder = VisGridderFactory::make(this->itsParset);

                // Define an imaging equation with an empty model (or pick one up from the parset)
                ASKAPLOG_INFO_STR(logger,"Building FFT/measurement equation");
                boost::shared_ptr<ImageEquation> imgEq(new ImageEquation (*itsModel, it, gridder, itsParset));
                ASKAPDEBUGASSERT(imgEq);

                // Leave this set to an empty pointer - no aggregation across groups for this
                // FIXME: This will be needed if we ever do distributed Taylor terms in the way they are done in
                //        yandasoft, with different terms on different ranks. However I believe the CDR plan for SKA
                //        is have a single gridder per frequency, and apply the MFS weighting during multiple image
                //        reductions. In that case all Taylor term degriddings operations for a given channel may
                //        be done on the same node.
                //imgEq->setVisUpdateObject(boost::shared_ptr<askap::synthesis::GroupVisAggregator>());

                // Eventually the following static vector cache will need to be replaced by VisData IO ports to
                // use the full functionality of Daliuge and avoid preventing certain deployment patterns from working.
                // The should just be a matter of having a single, non-static VisData::ShPtr that is connected to
                // the ports and fed to imgEq.
                //  - Just need to sort out blob strings for the GD and GDP classes. 
                // For now, use a static vector cache with one VisData::ShPtr for each parallel process.
                //  - In MPI-like mode, only one of the pointers would be set for a given process.
                //  - in a Daliuge-like mode, the pointers for all processes on a given node would be set.
                if (itsVisData.size() == 0) {
                    // with the lock above, only one of the parallel copies should get here
                    // @todo get maxNproc (or actual Nproc) properly. From scatter drop or parset?
                    const int maxNproc = NEUtils::getNChan(this->itsParset);
                    ASKAPLOG_INFO_STR(logger,"Resizing the VisData cache to "<<maxNproc);
                    itsVisData.resize(maxNproc);
                }
                // if this copy's cache has not been filled with data partitions, do it now
                if (itsVisData[this->itsRank] == nullptr) {
                    ASKAPLOG_INFO_STR(logger,"filling VisData cache for channel "<<this->itsRank);
                    itsVisData[this->itsRank] = imgEq->partitionData();
                }
                // set the current VisData object as the one to use for gridding
                ASKAPLOG_INFO_STR(logger,"Locking in the VisData cache for channel "<<this->itsRank);
                imgEq->setDataPartitions(itsVisData[this->itsRank]);

                // Generate the imaging normal equations (grid and FFT)
                ASKAPLOG_INFO_STR(logger,"calculating Imaging Normal Equations");
                imgEq->calcImagingEquations(*ne);

            } else {
                ASKAPTHROW(AskapError, "gridder.dataaccess type \""<<dataAccess<<"\" not supported.");
            }

            // Send the resulting normal equation images to the output port
            NEUtils::sendNE(ne, output("Normal"));

        }

        // I am going to assume a single Ne output - even though I am not
        // merging in the above loop - I should tho.

        // see, e.g. ImagerParallel::calcNE() -> MEParallel::sendNE() -> MEParallel::reduceNE(ne)

        // This is just to test whether this works at all.

        stats.logSummary();

        return 0;
    }

    void CalcNE::data_written(const char *uid, const char *data, size_t n) {
        dlg_app_running();
    }

    void CalcNE::drop_completed(const char *uid, drop_status status) {
        dlg_app_done(APP_FINISHED);
    }

} // namespace

