/// @file GenerateGridData.cc
///
/// @details Implements a test method that uses the contents of the parset
/// to load in a measurement set and print a summary of its contents
///
/// @author Tim Cornwell <tim.cornwell@csiro.au>
/// @author Stephen Ord <stephen.ord@csiro.au>
/// @author Daniel Mitchell <daniel.mitchell@csiro.au>

// for logging
#define ASKAP_PACKAGE_NAME "GenerateGridData"
#include <string>
/// askap namespace
namespace askap {
/// @return version of the package
    std::string getAskapPackageVersion_GenerateGridData() {
        return std::string("GenerateGridData; ASKAPsoft==Unknown");

    }
}
/// The version of the package
#define ASKAP_PACKAGE_VERSION askap::getAskapPackageVersion_GenerateGridData()

#include <vector>
#include <mutex>

// Local includes
#include <daliuge/DaliugeApplication.h>
#include <factory/GenerateGridData.h>
#include <factory/NEUtils.h>

// Yandasoft Data Source and Iterator classes (common to all data access methods that use Yandasoft DataAccessors)
#include <dataaccess/TableDataSource.h>
#include <dataaccess/IDataIterator.h>
#include <dataaccess/SharedIter.h>
#include <dataaccess/IDataConverter.h>
#include <dataaccess/IDataSelector.h>

// Yandasoft DataAccessors, DataIterators and Gridders
#include <measurementequation/ImageFFTEquation.h>
#include <gridding/IVisGridder.h>
#include <gridding/VisGridderFactory.h>

// Yandasoft DataAccessors with local GriddingInfo and Gridders
#include <factory/ImageEquation.h>
#include <factory/gridders/IVisGridder.h>
#include <factory/gridders/VisGridderFactory.h>

// LOFAR ParameterSet
#include <Common/ParameterSet.h>
// LOFAR Blob
#include <Blob/BlobString.h>
#include <Blob/BlobOBufString.h>
#include <Blob/BlobIBufString.h>
#include <Blob/BlobOStream.h>
#include <Blob/BlobIStream.h>

// Base ASKAP
#include <askap/AskapLogging.h>
#include <askap/AskapError.h>
#include <askap/StatReporter.h>

ASKAP_LOGGER(logger, ".GenerateGridData");

// params helpers
#include <measurementequation/SynthesisParamsHelper.h>
#include <measurementequation/ImageParamsHelper.h>
#include <parallel/GroupVisAggregator.h>

#include <scimath/fitting/Params.h>

#include <string.h>
#include <sys/time.h>

namespace askap {

    GenerateGridData::GenerateGridData(dlg_app_info *raw_app) :
        DaliugeApplication(raw_app)
    {
        this->itsRank = NEUtils::getRank(raw_app->uid);
        if (this->itsRank < 0) {
            this->itsRank = 0;
        }
    }

    GenerateGridData::~GenerateGridData() {
    }

    DaliugeApplication::ShPtr GenerateGridData::createDaliugeApplication(dlg_app_info *raw_app)
    {
        return GenerateGridData::ShPtr(new GenerateGridData(raw_app));
    }

    int GenerateGridData::init(const char ***arguments) {
        ASKAP_LOGGER(logger, ".GenerateGridData.init");

        while (1) {

            const char **param = *arguments;

            // Sentinel
            if (param == NULL) {
                break;
            }

            arguments++;
        }

        return 0;
    }

    int GenerateGridData::run() {

#ifndef ASKAP_PATCHED
        static std::mutex safety;
#endif

        ASKAP_LOGGER(logger, ".GenerateGridData.run");

        askap::StatReporter stats;

        // Lets get the key-value-parset
        char buf[64*1024];

        if (has_input("Config")) {
            size_t n_read = input("Config").read(buf, 64*1024);
            if (n_read == 0) {
                ASKAPLOG_WARN_STR(logger, "Nothing read from input Config");
                return -1;
            }
        } else {
            ASKAPLOG_WARN_STR(logger, "No input Config");
            return -1;
        }

        LOFAR::ParameterSet parset(true);
        parset.adoptBuffer(buf);

        // we need to fill the local parset with parameters that maybe missing
        try {
#ifndef ASKAP_PATCHED
            std::lock_guard<std::mutex> guard(safety);
#endif
            NEUtils::finaliseParameters(parset, this->itsRank);
        }
        catch (std::runtime_error)
        {
            return -1;
        }
        this->itsParset = parset.makeSubset("Cimager.");

        const string dataAccess = this->itsParset.getString("gridder.dataaccess", "datapartitions");
        ASKAPCHECK(dataAccess == "datapartitions", "Only dataaccess=datapartitions is possible in Ingest");

        // DAM change this back. Didn't help...
        //boost::shared_ptr<VisInfo> visInfo(new VisInfo());
        //VisData::ShPtr visData;

        // DAM TODO @fixme sort out where lock_guards are actually needed. Overhaul all of the locks used in Jacal
        {
#ifndef ASKAP_PATCHED
            std::lock_guard<std::mutex> guard(safety);
#endif

            // Receive VisInfo from the input port
            ASKAPCHECK(has_input("VisInfo"), "GenerateGridData requires VisInfo input port");
            boost::shared_ptr<VisInfo> visInfo(new VisInfo());
            NEUtils::receiveVisInfo(visInfo, input("VisInfo"));
            ASKAPLOG_INFO_STR(logger, "Receiving and unpacking visInfo with "<<
                                      visInfo->nSample()<<" samples, "<<
                                      visInfo->nPol()<<" polarisations and "<<
                                      visInfo->nChannel()<<" channels");
            if (visInfo->convertedToImagePol()) {
                ASKAPLOG_INFO_STR(logger, "Partition has already been converted to image polarisations");
            }
//        }
//
//        {
//#ifndef ASKAP_PATCHED
//            std::lock_guard<std::mutex> guard(safety);
//#endif

            // build a gridder
            IVisGridder::ShPtr gridder = VisGridderFactory::make(this->itsParset);

            // Define an imaging equation with empty sky grids
            // First, create the empty model from the definition in the parameter set.
            askap::scimath::Params::ShPtr skyGrids(new scimath::Params(true));
            askap::synthesis::SynthesisParamsHelper::setUpImages(skyGrids, this->itsParset.makeSubset("Images."));
            // Then build the imaging equation
            ASKAPLOG_INFO_STR(logger,"Building FFT/measurement equation");
            boost::shared_ptr<ImageEquation> imgEq(new ImageEquation(*skyGrids, gridder, itsParset));
            ASKAPDEBUGASSERT(imgEq);

            // Generate Visdata object using VisInfo
            VisData::ShPtr visData;
            visData = imgEq->initImagingEquations(visInfo);
//        }
//
//        {
//#ifndef ASKAP_PATCHED
//            std::lock_guard<std::mutex> guard(safety);
//#endif

            // Send VisInfo to the output port
            if (has_output("GriddingInfo")) {
                NEUtils::sendGriddingInfo(visData->griddingInfo(0), output("GriddingInfo"));
            }

            // Send vis to the output port
            if (has_output("vis")) {
                NEUtils::sendVis(visData->griddingInfo(0)->sample(), output("vis"));
            }
        }

        // This is just to test whether this works at all.

        stats.logSummary();

        return 0;
    }

    void GenerateGridData::data_written(const char *uid, const char *data, size_t n) {
        dlg_app_running();
    }

    void GenerateGridData::drop_completed(const char *uid, drop_status status) {
        dlg_app_done(APP_FINISHED);
    }

} // namespace

