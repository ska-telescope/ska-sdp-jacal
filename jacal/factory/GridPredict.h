/// @file GridPredict.h
///
/// @brief Calculate model visibilities (vis predict)
/// @details This class forms a set of model visibilties by degridding a model
/// image.
///
/// @author Tim Cornwell <tim.cornwell@csiro.au>
/// @author Stephen Ord <stephen.ord@csiro.au>
/// @author Daniel Mitchell <tim.cornwell@csiro.au>

#ifndef ASKAP_FACTORY_GRIDPREDICT_H
#define ASKAP_FACTORY_GRIDPREDICT_H

#include "rename.h"
#include <daliuge/DaliugeApplication.h>

#include <factory/gridders/VisData.h>

#include <casacore/casa/Quanta/MVDirection.h>

#include <boost/shared_ptr.hpp>

// LOFAR ParameterSet
#include <Common/ParameterSet.h>

// Yandasoft includes
#include <scimath/fitting/Params.h>

namespace askap {

    /*!
    * \brief GridPredict
    * \brief Calculates Model Visiblities
    * \details This class forms a set of model visibilties by degridding a model
    *  image
    * \par EAGLE_START
    * \param category DynlibApp
    * \param[in] param/libpath Library Path/"%JACAL_SO%"/String/readonly/
    *     \~English The path to the JACAL librarc
    * \param[in] param/Arg01 Arg01/name=GridPredict/String/readonly/
    *     \~English
    * \param[in] port/Config Config/LOFAR::ParameterSet/
    *     \~English ParameterSet descriptor for the image solver
    * \param[in] port/Model Model/scimath::Params/
    *     \~English Params of solved normal equations
    * \param[in] port/GriddingInfo GriddingInfo/GriddingInfo/
    *     \~English Gridding metadata
    * \param[in] port/vis vis/std::vector<std::vector<std::complex<float> > >/
    *     \~English Visibility data
    * \param[out] port/vis vis/std::vector<std::vector<std::complex<float> > >/
    *     \~English Residual visibility data
    * \par EAGLE_END
    */
    class GridPredict : public DaliugeApplication
    {
    public:

        typedef boost::shared_ptr<GridPredict> ShPtr;

        GridPredict(dlg_app_info *raw_app);

        static inline std::string ApplicationName() { return "GridPredict";}

        virtual ~GridPredict();

        static DaliugeApplication::ShPtr createDaliugeApplication(dlg_app_info *raw_app);

        virtual int init(const char ***arguments);

        virtual int run();

        virtual void data_written(const char *uid, const char *data, size_t n);

        virtual void drop_completed(const char *uid, drop_status status);


        private:

            //! @brief The model
            //! @details contains a set of parameters - essentially the solution of the NE
            askap::scimath::Params::ShPtr itsModel;

            //! @brief Parameter set
            //! @details key value list of configuration options
            LOFAR::ParameterSet itsParset;

            // Its tangent point
            std::vector<casacore::MVDirection> itsTangent; ///< the tangent point of the current grid

            int itsRank;

    };

} // namespace askap


#endif //

