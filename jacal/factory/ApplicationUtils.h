/// @file ApplicationUtils.h

#ifndef ASKAP_FACTORY_APPLICATION_UTILS_H
#define ASKAP_FACTORY_APPLICATION_UTILS_H

#include <daliuge/DaliugeApplication.h>

namespace askap {


    /*!
     * @brief set of static utility functions common to multiple applications
     * @details These are just a set of static functions I use more than once
     */
    class ApplicationUtils
    {
    public:

        ApplicationUtils() = delete;
        ~ApplicationUtils() = delete;

        static const std::string nodeName();

        static void updateLoggerContext(const int procid = -1);

        private:
    };

} // namespace askap


#endif //
