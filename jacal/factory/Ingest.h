/// @file Ingest.h
///
/// @brief Calculate imaging Normal Equations (vis predict followed by invert)
/// @details This class encorporates all of the tasks needed to form imaging
/// Normal Equations: read from a measurement set; degrid model visibilities;
/// subtract model visibilities; grid residual visibilities and FFT the grid
///
/// @author Tim Cornwell <tim.cornwell@csiro.au>
/// @author Stephen Ord <stephen.ord@csiro.au>
/// @author Daniel Mitchell <tim.cornwell@csiro.au>

#ifndef ASKAP_FACTORY_INGEST_H
#define ASKAP_FACTORY_INGEST_H

#include "rename.h"
#include <daliuge/DaliugeApplication.h>

#include <factory/gridders/VisData.h>

#include <casacore/casa/Quanta/MVDirection.h>

#include <boost/shared_ptr.hpp>
#include <boost/optional.hpp>

// LOFAR ParameterSet
#include <Common/ParameterSet.h>

// Yandasoft includes
#include <scimath/fitting/Params.h>

namespace askap {

    /*!
    * \brief Ingest
    * \brief Calculates the Normal Equations
    * \details This class encorporates all of the tasks needed to form imaging
    *  Normal Equations: read from a measurement set; degrid model visibilities;
    *  subtract model visibilities; grid residual visibilities and FFT the grid
    * \par EAGLE_START
    * \param category DynlibApp
    * \param[in] param/libpath Library Path/"%JACAL_SO%"/String/readonly/
    *     \~English The path to the JACAL librarc
    * \param[in] param/Arg01 Arg01/name=Ingest/String/readonly/
    *     \~English
    * \param[in] port/Config Config/LOFAR::ParameterSet/
    *     \~English ParameterSet descriptor for the image solver
    * \param[out] port/visVector visVector/std::vector<float>/
    *     \~English Visiblity data
    * \param[out] port/VisInfo VisInfo/visibilityData/
    *     \~English Visiblity metadata
    * \par EAGLE_END
    */
    class Ingest : public DaliugeApplication
    {
    public:

        typedef boost::shared_ptr<Ingest> ShPtr;

        Ingest(dlg_app_info *raw_app);

        static inline std::string ApplicationName() { return "Ingest";}

        virtual ~Ingest();

        static DaliugeApplication::ShPtr createDaliugeApplication(dlg_app_info *raw_app);

        virtual int init(const char ***arguments);

        virtual int run();

        virtual void data_written(const char *uid, const char *data, size_t n);

        virtual void drop_completed(const char *uid, drop_status status);


        private:

            //! @brief Parameter set
            //! @details key value list of configuration options
            LOFAR::ParameterSet itsParset;

            // Its tangent point
            std::vector<casacore::MVDirection> itsTangent;

            int itsRank;

            // utility to build an Imaging Normal Equation from a parset
            // void buildNE();

            // these are the steps required by buildNE

    };

} // namespace askap


#endif //

