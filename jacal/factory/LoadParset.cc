/// @file LoadParset.cc
///
/// @brief Load a LOFAR Parameter Set in the DaliugeApplication Framework
/// @details Loads a configuration from a file drop and generates a LOFAR::ParameterSet
/// The first ASKAP example in the Daliuge framework that actually
/// performs an ASKAP related task.
/// We load a parset into memory from either a file or another Daliuge drop_status

// for logging
#define ASKAP_PACKAGE_NAME "LoadParset"
#include <string>
/// askap namespace
namespace askap {
/// @return version of the package
    std::string getAskapPackageVersion_LoadParset() {
        return std::string("LoadParset; ASKAPsoft==Unknown");

    }
}
/// The version of the package
#define ASKAP_PACKAGE_VERSION askap::getAskapPackageVersion_LoadParset()

#include <daliuge/DaliugeApplication.h>
#include <factory/LoadParset.h>
#include <factory/NEUtils.h>

#include <askap/AskapLogging.h>
#include <askap/AskapError.h>

// LOFAR ParameterSet
#include <Common/ParameterSet.h>

#include <string.h>
#include <sys/time.h>


namespace askap {

    LoadParset::LoadParset(dlg_app_info *raw_app) :
        DaliugeApplication(raw_app)
    {
    }

    LoadParset::~LoadParset() {
    }

    DaliugeApplication::ShPtr LoadParset::createDaliugeApplication(dlg_app_info *raw_app)
    {
        return LoadParset::ShPtr(new LoadParset(raw_app));
    }

    int LoadParset::init(const char ***arguments) {
        ASKAP_LOGGER(logger, ".LoadParset.init");

        char *parset_filename = 0;
        while (1) {

            const char **param = *arguments;

            // Sentinel
            if (param == NULL) {
                break;
            }
            // any params I might need go here:
            // filename:
            // no longer required as input comes from daliuge now
            //if (strcmp(param[0], "parset_filename") == 0) {
            //    parset_filename = strdup(param[1]);
            //}
            if (strcmp(param[0], "num_of_copies") == 0) {
                ASKAPLOG_DEBUG_STR(logger, "extracting scatter parameter "<<param[0]<<" = "<<param[1]);
                its_num_of_copies = std::stoi(param[1]);
            }
            if (strcmp(param[0], "scatter_axis") == 0) {
                ASKAPLOG_DEBUG_STR(logger, "extracting scatter parameter "<<param[0]<<" = "<<param[1]);
                its_scatter_axis = strdup(param[1]);
            }

            arguments++;
        }

        return 0;
    }

    int LoadParset::run() {

        ASKAP_LOGGER(logger, ".LoadParset.run");

        // lets open the input and read it
        char buf[64*1024];
        size_t n_read = input(0).read(buf, 64*1024);

        LOFAR::ParameterSet parset(true);
        parset.adoptBuffer(buf);

        if (its_num_of_copies) {
            const std::string param("Daliuge.Scatter.num_of_copies");
            parset.add(param,std::to_string(*its_num_of_copies));
            ASKAPLOG_INFO_STR(logger, "added "<<param<<" = "<<parset.getInt(param)<<" to parset");
        }
        if (its_scatter_axis) {
            const std::string param("Daliuge.Scatter.scatter_axis");
            parset.add(param,*its_scatter_axis);
            ASKAPLOG_INFO_STR(logger, "added "<<param<<" = "<<parset.getString(param)<<" to parset");
        }
        std::string sbuf;
		parset.writeBuffer(sbuf);

        // write it to the outputs
        for (int i = 0; i < n_outputs(); i++) {
            //output(i).write(buf, n_read);
            output(i).write(sbuf.c_str(), strlen(sbuf.c_str()));
        }

        return 0;
    }


    void LoadParset::data_written(const char *uid, const char *data, size_t n) {
        dlg_app_running();
    }

    void LoadParset::drop_completed(const char *uid, drop_status status) {
        dlg_app_done(APP_FINISHED);
    }


} // namespace
