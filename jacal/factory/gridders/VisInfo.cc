/// @file VisInfo.cc
///
/// @brief 
/// @details 
///
/// @author Daniel Mitchell <daniel.mitchell@csiro.au>

// for logging
#define ASKAP_PACKAGE_NAME "VisInfo"
#include <string>
/// askap namespace
namespace askap {
/// @return version of the package
    std::string getAskapPackageVersion_VisInfo() {
        return std::string("VisInfo; ASKAPsoft==Unknown");

    }
}
/// The version of the package
#define ASKAP_PACKAGE_VERSION askap::getAskapPackageVersion_VisInfo()

// Base ASKAP
#include <askap/AskapLogging.h>
#include <askap/AskapError.h>
ASKAP_LOGGER(logger, ".VisInfo");
#include <profile/AskapProfiler.h>

// Base Accessors
#include <dataaccess/IConstDataAccessor.h>

// Local includes
#include <factory/gridders/VisInfo.h>

#include <casacore/casa/BasicSL/Constants.h>

#include <Blob/BlobOStream.h>
#include <Blob/BlobIStream.h>
#include <Blob/BlobArray.h>
#include <Blob/BlobSTL.h>

namespace askap {

VisInfo::VisInfo() :
                      itsNumberOfSamples(0), itsNumberOfPols(0), itsNumberOfChannels(0),
                      itsConvertedToImagePol(false) {}

/// add samples from a const data accessor
/// @param acc a reference to the associated accessor
void VisInfo::addData(const accessors::IConstDataAccessor& acc)
{
    // @todo this could possibly be done inside each gridder to be fit for purpose

    if (acc.nRow() * acc.nChannel() == 0) {
        ASKAPLOG_WARN_STR(logger, "Nothing to add. nRow = "<<acc.nRow()<<", nChannel = "<<acc.nChannel());
        return;
    }

    const int sample0 = itsNumberOfSamples;

    itsNumberOfSamples += acc.nRow() * acc.nChannel();

    const casacore::Vector<casacore::Double> freq = acc.frequency();

    if (sample0 == 0) {
        itsNumberOfPols = acc.nPol();
        itsStokes = acc.stokes();
        itsNumberOfChannels = acc.nChannel();
        itsFrequencyList.resize(itsNumberOfChannels);
        itsConvertedToImagePol = false;
        for (uint chan=0; chan<itsNumberOfChannels; ++chan) itsFrequencyList[chan] = freq(chan);
    } else {
        ASKAPCHECK(itsNumberOfPols == acc.nPol(),
                "Incompatible number of polarisations: "<<itsNumberOfPols<<" != "<<acc.nPol());
        ASKAPCHECK(itsNumberOfChannels == acc.nChannel(),
                "Incompatible number of channels: "<<itsNumberOfChannels<<" != "<<acc.nChannel());
    }

    const casacore::Vector<casacore::RigidVector<double, 3> > &uvw = acc.rotatedUVW(itsTangentPoint);
    const casa::Vector<double> &delay = acc.uvwRotationDelay(itsTangentPoint, itsImageCentre);
    //const casacore::Vector<casacore::RigidVector<double, 3> > &uvw = acc.uvw();
    const casacore::Vector<casacore::MVDirection> &pointingDir1 = acc.pointingDir1();
    const casacore::Vector<casacore::MVDirection> &pointingDir2 = acc.pointingDir2();
    const casacore::Vector<casacore::Float> &feed1PA = acc.feed1PA();
    const casacore::Vector<casacore::Float> &feed2PA = acc.feed2PA();

    const casacore::Vector<casacore::Double> reciprocalToWavelength = acc.frequency()/casacore::C::c;

    itsU.resize(itsNumberOfSamples);
    itsV.resize(itsNumberOfSamples);
    itsW.resize(itsNumberOfSamples);
    itsPhasor.resize(itsNumberOfSamples);
    itsChannel.resize(itsNumberOfSamples);
    itsFlag.resize(itsNumberOfSamples);
    itsPointingDir1.resize(itsNumberOfSamples);
    itsPointingDir2.resize(itsNumberOfSamples);
    itsFeed1PA.resize(itsNumberOfSamples);
    itsFeed2PA.resize(itsNumberOfSamples);
    itsNoise.resize(itsNumberOfSamples);
    itsWeight.resize(itsNumberOfSamples);
    itsSample.resize(itsNumberOfSamples);
    //itsModel.resize(itsNumberOfSamples);
    for (uint i=0; i<acc.nRow(); ++i) {
        for (uint chan=0; chan<acc.nChannel(); ++chan) {
            const uint index = sample0 + i*acc.nChannel() + chan;
            itsU[index] = uvw(i)(0)*reciprocalToWavelength(chan);
            itsV[index] = uvw(i)(1)*reciprocalToWavelength(chan);
            itsW[index] = uvw(i)(2)*reciprocalToWavelength(chan);
            // this is delay associated with uvw rotation (see above)
            const double phase = 2.0f*casacore::C::pi * delay[i] * reciprocalToWavelength(chan);
            itsPhasor[index] = casacore::Complex(cos(phase), sin(phase));
            //itsPhasor[index] = 1.0;
            itsChannel[index] = chan;
            //itsFlag[index] = false;
            itsFlag[index] = 0;
            itsPointingDir1[index] = pointingDir1(i);
            itsPointingDir2[index] = pointingDir2(i);
            itsFeed1PA[index] = feed1PA(i);
            itsFeed2PA[index] = feed2PA(i);
            itsNoise[index].resize(itsNumberOfPols);
            itsWeight[index].resize(itsNumberOfPols);
            itsSample[index].resize(itsNumberOfPols);
            //itsModel[index].resize(itsNumberOfPols);
            /// @todo flagging could be done here as well
            /// swap order with sample loop? We do reset the pol shape later though...
            for (uint pol=0; pol<itsNumberOfPols; ++pol) {
                // @todo move all of these references out of the loops
                // if any polarisation is flagged, flag them all
                //if (acc.flag()(i,chan,pol)) itsFlag[index] = true;
                if (acc.flag()(i,chan,pol)) itsFlag[index] = 1;
                const float noise = real(acc.noise()(i,chan,pol));
                itsNoise[index][pol] = noise;
                // this will be overwritten if itsNoise changes (e.g. in polarisation conversion)
                itsWeight[index][pol] = (noise > 0.) ? 1./(noise*noise) : 0.;
                itsSample[index][pol] = acc.visibility()(i,chan,pol);
                //itsModel[index][pol] = 0.0;
            }
        }
    }

}

/// add samples from a const data accessor
/// @param acc a reference to the associated accessor
void VisInfo::copySamples(boost::shared_ptr<VisInfo> &in_ptr,
                                        const std::vector<size_t> &idx)
{

    itsNumberOfSamples = idx.size();
    itsNumberOfChannels = in_ptr->itsNumberOfChannels;
    itsNumberOfPols = in_ptr->itsNumberOfPols;
    itsConvertedToImagePol = in_ptr->itsConvertedToImagePol;
    itsStokes = in_ptr->itsStokes;
    itsFrequencyList = in_ptr->itsFrequencyList;

    itsU.resize(itsNumberOfSamples);
    itsV.resize(itsNumberOfSamples);
    itsW.resize(itsNumberOfSamples);
    itsPhasor.resize(itsNumberOfSamples);
    itsChannel.resize(itsNumberOfSamples);
    itsFlag.resize(itsNumberOfSamples);
    itsPointingDir1.resize(itsNumberOfSamples);
    itsPointingDir2.resize(itsNumberOfSamples);
    itsFeed1PA.resize(itsNumberOfSamples);
    itsFeed2PA.resize(itsNumberOfSamples);
    itsNoise.resize(itsNumberOfSamples);
    itsWeight.resize(itsNumberOfSamples);
    itsSample.resize(itsNumberOfSamples);
    //itsModel.resize(itsNumberOfSamples);
    for (uint i=0; i<itsNumberOfSamples; ++i) {
        const uint index = i;
        itsU[i] = in_ptr->itsU[idx[i]];
        itsV[i] = in_ptr->itsV[idx[i]];
        itsW[i] = in_ptr->itsW[idx[i]];
        itsPhasor[i] = in_ptr->itsPhasor[idx[i]];
        itsChannel[i] = in_ptr->itsChannel[idx[i]];
        itsFlag[i] = in_ptr->itsFlag[idx[i]];
        itsPointingDir1[i] = in_ptr->itsPointingDir1[idx[i]];
        itsPointingDir2[i] = in_ptr->itsPointingDir2[idx[i]];
        itsFeed1PA[i] = in_ptr->itsFeed1PA[idx[i]];
        itsFeed2PA[i] = in_ptr->itsFeed2PA[idx[i]];
        itsNoise[i].resize(itsNumberOfPols);
        itsWeight[i].resize(itsNumberOfPols);
        itsSample[i].resize(itsNumberOfPols);
        //itsModel[i].resize(itsNumberOfPols);
        for (uint pol=0; pol<itsNumberOfPols; ++pol) {
            itsNoise[i][pol] = in_ptr->itsNoise[idx[i]][pol];
            itsWeight[i][pol] = in_ptr->itsWeight[idx[i]][pol];
            itsSample[i][pol] = in_ptr->itsSample[idx[i]][pol];
            //itsModel[i][pol] = in_ptr->itsModel[idx[i]][pol];
        }
    }

}

/// Convert buffered vis data and weights to the imaging polarisation frame
/// @TODO incorporate this into addData?
void VisInfo::convertPol(const casacore::Vector<casacore::Stokes::StokesTypes>& imagePols) {

    ASKAPDEBUGTRACE("VisInfo::convertPol");

    if (convertedToImagePol()) {
        return;
    }

    // if the input and output stokes parameters are the same, leave as is
    if (allEQ(itsStokes, imagePols)) {
        convertedToImagePol(true);
        return;
    }

    ASKAPDEBUGASSERT(itsNumberOfPols == itsStokes.size());
    ASKAPDEBUGASSERT(itsFlag.size() == itsNumberOfSamples);
    ASKAPDEBUGASSERT(itsNoise.size() == itsNumberOfSamples);
    ASKAPDEBUGASSERT(itsWeight.size() == itsNumberOfSamples);
    ASKAPDEBUGASSERT(itsSample.size() == itsNumberOfSamples);

    // set up for the polarisation conversion

    // converter
    // for now setup the converter inside this method, although it would cause a rebuild
    // of the matrices for every partition. More intelligent caching is possible with a bit
    // more effort (i.e. one has to detect whether polarisation frames change from the
    // previous call). Need to think about parallactic angle dependence.
    scimath::PolConverter itsPolConv = scimath::PolConverter(itsStokes, imagePols);

    // input buffers
    casacore::Vector<casacore::Complex> visPolFrameVis(itsNumberOfPols);

    // output buffers
    const casacore::uInt nImagePols = imagePols.nelements();
    casacore::Vector<casacore::Complex> imagePolFrameVis(nImagePols);
    casacore::Vector<casacore::Complex> imagePolFrameNoise(nImagePols);

    // Loop over all samples and convert to imaging polarisation coordinates
    bool givenFlagWarning = false;

    for (uint i=0; i<itsNumberOfSamples; ++i) {

        if (itsFlag[i]) continue;

        for (uint pol=0; pol<itsNumberOfPols; pol++) visPolFrameVis(pol) = itsSample[i][pol];
        itsPolConv.convert(imagePolFrameVis,visPolFrameVis);
        itsSample[i].resize(nImagePols);
        for (uint pol=0; pol<nImagePols; pol++) itsSample[i][pol] = imagePolFrameVis(pol);

        for (uint pol=0; pol<itsNumberOfPols; pol++) visPolFrameVis(pol) = itsNoise[i][pol];
        itsPolConv.noise(imagePolFrameNoise,visPolFrameVis);
        itsNoise[i].resize(nImagePols);
        for (uint pol=0; pol<nImagePols; pol++) itsNoise[i][pol] = real(imagePolFrameNoise(pol));

        /// it would be nice to swap sample,pol order, but this would be harder...
        itsWeight[i].resize(nImagePols);
        //itsModel[i].resize(nImagePols);

        // Now loop over all image polarizations
        for (uint pol=0; pol<nImagePols; ++pol) {
            // should perhaps flag this whole sample if any polarisations have zero weight...
            // @todo add a convertedToImagePol() check when generating robust weights so this isn't overwriting them
            if (itsNoise[i][pol] <= 0.) {
                //itsFlag[i] = true;
                itsFlag[i] = 1;
                break;
            }
            itsWeight[i][pol] = 1. / (itsNoise[i][pol]*itsNoise[i][pol]);
            //itsModel[i][pol] = 0.0;
        }

        // Check if anything has been flagged since starting this loop
        if (itsFlag[i]) {
            // This shouldn't happen but also shouldn't hurt, so warn and carry on
            if (!givenFlagWarning) {
                ASKAPLOG_WARN_STR(logger, "Flagging zero-weight sample after pol-conv. Suppressing further warnings.");
            }
            givenFlagWarning = true;
            continue;
        }

    }

    // reset buffer polarisation vector
    itsStokes.assign(imagePols);    
    resetNPol();

    convertedToImagePol(true);

}

// These are the items that we need to write to and read from a blob stream
// std::map<std::string, casacore::Array<double> > itsArrays;
// std::map<std::string, Axes> itsAxes;
// std::map<std::string, bool> itsFree;

#define BLOBVERSION 0

LOFAR::BlobOStream& operator<<(LOFAR::BlobOStream& os, const VisInfo& info)
{
    os.putStart("VisInfo",BLOBVERSION);
    os << info.itsNumberOfSamples
       << info.itsNumberOfChannels
       << info.itsNumberOfPols
       << info.itsConvertedToImagePol
       //<< info.itsStokes
       << info.itsFrequencyList
       << info.itsU
       << info.itsV
       << info.itsW
       << info.itsPhasor
       << info.itsChannel
       << info.itsFlag
       //<< info.itsPointingDir1
       //<< info.itsPointingDir2
       << info.itsFeed1PA
       << info.itsFeed2PA
       << info.itsNoise
       << info.itsWeight
       << info.itsSample;
	os.putEnd();
    return os;
}

LOFAR::BlobIStream& operator>>(LOFAR::BlobIStream& is, VisInfo& info)
{
    // info should still have default constructor settings
    ASKAPDEBUGASSERT(info.itsNumberOfSamples == 0);
    ASKAPDEBUGASSERT(info.itsNumberOfChannels == 0);
    ASKAPDEBUGASSERT(info.itsNumberOfPols == 0);
    ASKAPDEBUGASSERT(info.itsConvertedToImagePol == false);
    const int version = is.getStart("VisInfo");
    ASKAPCHECK(version == BLOBVERSION,
        "Attempting to read from a blob stream for a VisInfo object of the wrong version, expect "<<
        BLOBVERSION<<" got "<<version);

    // first get size parameters and resize the vectors
    is >> info.itsNumberOfSamples
       >> info.itsNumberOfChannels
       >> info.itsNumberOfPols
       >> info.itsConvertedToImagePol;

    ASKAPDEBUGASSERT(info.itsNumberOfSamples > 0);
    ASKAPDEBUGASSERT(info.itsNumberOfChannels > 0);
    ASKAPDEBUGASSERT(info.itsNumberOfPols > 0);

    //info.itsStokes.resize(info.itsNumberOfPols);
    info.itsFrequencyList.resize(info.itsNumberOfChannels);
    info.itsU.resize(info.itsNumberOfSamples);
    info.itsV.resize(info.itsNumberOfSamples);
    info.itsW.resize(info.itsNumberOfSamples);
    info.itsPhasor.resize(info.itsNumberOfSamples);
    info.itsChannel.resize(info.itsNumberOfSamples);
    info.itsFlag.resize(info.itsNumberOfSamples);
    //info.itsPointingDir1.resize(info.itsNumberOfSamples);
    //info.itsPointingDir2.resize(info.itsNumberOfSamples);
    info.itsFeed1PA.resize(info.itsNumberOfSamples);
    info.itsFeed2PA.resize(info.itsNumberOfSamples);
    info.itsNoise.resize(info.itsNumberOfSamples);
    info.itsWeight.resize(info.itsNumberOfSamples);
    info.itsSample.resize(info.itsNumberOfSamples);
    for (uint i=0; i<info.itsNumberOfSamples; ++i) {
        info.itsNoise[i].resize(info.itsNumberOfPols);
        info.itsWeight[i].resize(info.itsNumberOfPols);
        info.itsSample[i].resize(info.itsNumberOfPols);
    }

    is //>> info.itsStokes
       >> info.itsFrequencyList
       >> info.itsU
       >> info.itsV
       >> info.itsW
       >> info.itsPhasor
       >> info.itsChannel
       >> info.itsFlag
       //>> info.itsPointingDir1
       //>> info.itsPointingDir2
       >> info.itsFeed1PA
       >> info.itsFeed2PA
       >> info.itsNoise
       >> info.itsWeight
       >> info.itsSample;
    is.getEnd();
    return is;
}

VisInfo::~VisInfo()
{
}

} // end of namespace askap
