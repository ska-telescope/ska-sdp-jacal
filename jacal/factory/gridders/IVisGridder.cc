/// @file IVisGridder.cc
///
/// @brief Base class for visibility gridders.

// for logging
#define ASKAP_PACKAGE_NAME "IVisGridder"
#include <string>
/// askap namespace
namespace askap {
/// @return version of the package
    std::string getAskapPackageVersion_IVisGridder() {
        return std::string("IVisGridder; ASKAPsoft==Unknown");

    }
}
/// The version of the package
#define ASKAP_PACKAGE_VERSION askap::getAskapPackageVersion_IVisGridder()

#include "factory/gridders/IVisGridder.h"

// Base ASKAP
#include <askap/AskapLogging.h>
#include <askap/AskapError.h>
ASKAP_LOGGER(logger, ".IVisGridder");

namespace askap
{

    IVisGridder::IVisGridder()
    {
    }

    IVisGridder::~IVisGridder()
    {
    }
    
    /// @brief static method to create gridder
	/// @details Each gridder should have a static factory method, which is
	/// able to create a particular type of the gridder and initialise it with
	/// the parameters taken form the given parset. It is assumed that the 
	/// method receives a subset of parameters where the gridder name is already
	/// taken out. 
	/// @note This method just throws an exception in this basic interface. It is 
	/// added to ensure that all derived classes have this method defined. We have 
	/// to use a static method as opposed to pure virtual function because we plan to use it
	/// to create a brand new instance of the gridder (and hence no object would
	/// exist at that stage)			 
	IVisGridder::ShPtr IVisGridder::createGridder(const LOFAR::ParameterSet&) 
    {
       ASKAPTHROW(AskapError, "createGridder is supposed to be defined for every derived gridder, "
                              "IVisGridder::createGridder should never be called");           
       return IVisGridder::ShPtr();                                            
    }

}
