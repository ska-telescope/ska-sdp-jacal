/// @file VisData.cc
///
/// @brief 
/// @details 
///
/// @author Daniel Mitchell <daniel.mitchell@csiro.au>

// for logging
#define ASKAP_PACKAGE_NAME "VisData"
#include <string>
/// askap namespace
namespace askap {
/// @return version of the package
    std::string getAskapPackageVersion_VisData() {
        return std::string("VisData; ASKAPsoft==Unknown");

    }
}
/// The version of the package
#define ASKAP_PACKAGE_VERSION askap::getAskapPackageVersion_VisData()

// Base ASKAP
#include <askap/AskapLogging.h>
#include <askap/AskapError.h>
ASKAP_LOGGER(logger, ".VisData");

// Local includes
#include <factory/gridders/VisData.h>
#include <factory/OnDemandBufferDataAccessor.h>

#include <askap/CasaSyncHelper.h>
#include <askap/AskapUtil.h>

//#include <scimath/utils/PaddingUtils.h>
//#include <scimath/utils/ImageUtils.h>

#include <casacore/casa/BasicSL/Constants.h>

#include <numeric>      // std::iota
#include <algorithm>    // std::sort, std::stable_sort

namespace askap {

/// @brief required to mediate thread safety issues of the casa cube
utility::CasaSyncHelper GDsyncHelper;

/// construct an object linked with the given iterator data iterator
/// @param idi a reference to the associated accessor
VisData::VisData(const accessors::IDataSharedIter &idi, const scimath::Axes &axes,
                           const casacore::IPosition &shape, const std::string partitionType) :
                      itsNumberOfPartitions(0)
{
    ASKAP_LOGGER(logger, ".VisData.VisData");

    if (!itsVisInfo.empty()) {
        itsNumberOfPartitions = itsVisInfo.size();
        ASKAPLOG_INFO_STR(logger, "Using VisInfo cache with "<<itsNumberOfPartitions<<" partitions.");
        // do some tests...
        //  - check that itsGriddingInfo is consistent
    } else {

        ASKAPLOG_INFO_STR(logger, "Filling VisInfo with partition type \""<<partitionType<<"\"");

        itsAxes = axes;
        itsShape = shape;
        // TODO: This is set in the gridder so isn't available here. Need to fix that.
        itsPaddingFactor = 1.0;

        ASKAPLOG_INFO_STR(logger, "Gridding to be set up with tangent centre "<<
            printDirection(getTangentPoint())<<" and image centre "<<printDirection(getImageCentre()));

        // accumulate all of the time-ordered data-accessor iterations into a single VisInfo
        boost::shared_ptr<VisInfo> tmpPartition = boost::shared_ptr<VisInfo>(new VisInfo());
        tmpPartition->setImageCentre(getImageCentre());
        tmpPartition->setTangentPoint(getTangentPoint());
        for (idi.init();idi.hasMore();idi.next()) {
            OnDemandBufferDataAccessor acc(*idi);
            tmpPartition->addData(acc);
        }

        itsNumberOfPartitions = 1;
        itsVisInfo.resize(itsNumberOfPartitions);
        itsVisInfo[0] = boost::shared_ptr<VisInfo>(new VisInfo());

        // construct this here, but allocate memory for the vectors later
        itsGriddingInfo.resize(itsNumberOfPartitions);
        itsGriddingInfo[0] = boost::shared_ptr<GriddingInfo>(new GriddingInfo());

        if (partitionType == "time") {
            itsVisInfo[0] = tmpPartition;
        } else if (partitionType == "wsort") {
            // determine the w-sorted indices of all of the samples in the first partition
            std::vector<size_t> idx(tmpPartition->nSample());
            iota(idx.begin(), idx.end(), 0);
            const std::vector<double> &w = tmpPartition->w();
            stable_sort(idx.begin(), idx.end(), [&w](size_t i1, size_t i2) {return w[i1] < w[i2];});

            // sort data into the main vector VisInfo pointers
            const int samplesPerPartition = tmpPartition->nSample() / itsNumberOfPartitions;
            itsVisInfo.resize(itsNumberOfPartitions);
            ASKAPLOG_DEBUG_STR(logger, "copying partition with w range: "<<w[idx.front()]<<" - "<<w[idx.back()]);
            itsVisInfo[0]->copySamples(tmpPartition,idx);
        } else {
            ASKAPTHROW(AskapError, "Unknown partitionType: " << partitionType);
        }

        ASKAPLOG_INFO_STR(logger, "Added "<<itsNumberOfPartitions<<" data partitions");

    }

}

/// construct an object for a visInfo
/// @param visInfo a shared_ptr to the associated visInfo
VisData::VisData(boost::shared_ptr<VisInfo> &visInfo, const scimath::Axes &axes,
                 const casacore::IPosition &shape) :
                      itsNumberOfPartitions(1)
{
    ASKAP_LOGGER(logger, ".VisData.VisData");

    ASKAPCHECK(itsVisInfo.empty(), "Initialising a non-empty VisData object");

    ASKAPLOG_INFO_STR(logger, "Filling VisInfo with existing visInfo");

    itsAxes = axes;
    itsShape = shape;
    // TODO: This is set in the gridder so isn't available here. Need to fix that.
    itsPaddingFactor = 1.0;

    itsVisInfo.resize(itsNumberOfPartitions);
    itsVisInfo[0] = boost::shared_ptr<VisInfo>(new VisInfo());
    itsVisInfo[0] = visInfo;

    // construct this here, but allocate memory for the vectors later
    itsGriddingInfo.resize(itsNumberOfPartitions);
    itsGriddingInfo[0] = boost::shared_ptr<GriddingInfo>(new GriddingInfo());

    // DAM this has already been done, but that may change in the near future, so keep them here for now
    //itsVisInfo[0]->setImageCentre(getImageCentre());
    //itsVisInfo[0]->setTangentPoint(getTangentPoint());

    ASKAPLOG_INFO_STR(logger, "Gridding should be set up with tangent centre "<<
        printDirection(getTangentPoint())<<" and image centre "<<printDirection(getImageCentre()));

    ASKAPLOG_INFO_STR(logger, "Added "<<itsNumberOfPartitions<<" data partitions");

}

/// construct an object for a visInfo
/// @param visInfo a shared_ptr to the associated visInfo
VisData::VisData(boost::shared_ptr<GriddingInfo> &griddingInfo, const scimath::Axes &axes,
                 const casacore::IPosition &shape) :
                      itsNumberOfPartitions(1)
{
    ASKAP_LOGGER(logger, ".VisData.VisData");

    ASKAPCHECK(itsVisInfo.empty(), "Initialising a non-empty VisData object");

    ASKAPLOG_INFO_STR(logger, "Filling VisInfo with existing visInfo");

    itsAxes = axes;
    itsShape = shape;
    // TODO: This is set in the gridder so isn't available here. Need to fix that.
    itsPaddingFactor = 1.0;

    itsVisInfo.resize(itsNumberOfPartitions);
    itsVisInfo[0] = boost::shared_ptr<VisInfo>(new VisInfo());

    // construct this here, but allocate memory for the vectors later
    itsGriddingInfo.resize(itsNumberOfPartitions);
    itsGriddingInfo[0] = boost::shared_ptr<GriddingInfo>(new GriddingInfo());
    itsGriddingInfo[0] = griddingInfo;

    // DAM this has already been done, but that may change in the near future, so keep them here for now
    //itsVisInfo[0]->setImageCentre(getImageCentre());
    //itsVisInfo[0]->setTangentPoint(getTangentPoint());

    ASKAPLOG_INFO_STR(logger, "Gridding should be set up with tangent centre "<<
        printDirection(getTangentPoint())<<" and image centre "<<printDirection(getImageCentre()));

    ASKAPLOG_INFO_STR(logger, "Added "<<itsNumberOfPartitions<<" data partitions");

}

VisData::~VisData()
{
}

/// @brief obtain the centre of the image
/// @details This method extracts RA and DEC axes from itsAxes and
/// forms a direction measure corresponding to the middle of each axis.
/// @return direction measure corresponding to the image centre
casacore::MVDirection VisData::getImageCentre() const
{
    ASKAP_LOGGER(logger, ".VisData.getImageCentre");
    ASKAPCHECK(itsAxes.hasDirection(),"Direction axis is missing. axes="<<itsAxes);
    casacore::MDirection out;
    casacore::Vector<casacore::Double> centrePixel(2);
    ASKAPDEBUGASSERT(itsShape.nelements()>=2);
    ASKAPDEBUGASSERT(paddingFactor()>0);
    for (size_t dim=0; dim<2; ++dim) {
         centrePixel[dim] = double(itsShape[dim])/2./double(paddingFactor());
    }
    ASKAPCHECK(GDsyncHelper.toWorld(itsAxes.directionAxis(),out, centrePixel),
         "Unable to obtain world coordinates for the centre of the image. Something is wrong with the coordinate system");
    return out.getValue();
}

/// @brief obtain the tangent point
/// @details For faceting all images should be constructed for the same tangent
/// point. This method extracts the tangent point (reference position) from the
/// coordinate system.
/// @return direction measure corresponding to the tangent point
casacore::MVDirection VisData::getTangentPoint() const
{
    ASKAP_LOGGER(logger, ".VisData.getTangentPoint");
    ASKAPCHECK(itsAxes.hasDirection(),"Direction axis is missing. axes="<<itsAxes);
    const casacore::Vector<casacore::Double> refVal(itsAxes.directionAxis().referenceValue());
    ASKAPDEBUGASSERT(refVal.nelements() == 2);
    const casacore::Quantum<double> refLon(refVal[0], "rad");
    const casacore::Quantum<double> refLat(refVal[1], "rad");
    const casacore::MVDirection out(refLon, refLat);
    return out;
}

/// @brief calculate stokes pol vector using information from itsAxes and itsShape
casacore::Vector<casacore::Stokes::StokesTypes> VisData::getImagePols()
{
    ASKAP_LOGGER(logger, ".VisData.getImagePols");
    casacore::Vector<casacore::Stokes::StokesTypes> stokes;
    const int nPol = itsShape.nelements()>=3 ? itsShape[2] : 1;
    if (itsAxes.has("STOKES")) {
        stokes = itsAxes.stokesAxis();
    } else {
        stokes.resize(1);
        stokes[0] = casacore::Stokes::I;
    }
    ASKAPCHECK(int(stokes.nelements()) == nPol,
        "Stokes axis is not consistent with the shape of the grid. There are "<<nPol<<
        " planes in the grid and "<<stokes.nelements()<<" polarisation descriptors defined by the STOKES axis");
    return stokes;
}

} // end of namespace askap
