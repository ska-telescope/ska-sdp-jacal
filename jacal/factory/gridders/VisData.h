/// @file VisData.h
///
/// @brief 
/// @details 
///
/// @author Daniel Mitchell <daniel.mitchell@csiro.au>

#ifndef ASKAP_FACTORY_GRIDDING_DATA_ACCESSOR_H
#define ASKAP_FACTORY_GRIDDING_DATA_ACCESSOR_H

#include <complex>
#include <vector>

// boost includes
#include <boost/shared_ptr.hpp>

#include <dataaccess/IDataIterator.h>
#include <dataaccess/SharedIter.h>

#include <factory/gridders/VisInfo.h>
#include <factory/gridders/GriddingInfo.h>

#include <scimath/fitting/Axes.h>

namespace askap {

/// @brief Interface class for read-only access to visibility data
/// @ingroup dataaccess_i
class VisData
{

public:

    /// construct an object linked with the given shared data iterator
    /// @param idi a reference to the associated iterator
    VisData(const accessors::IDataSharedIter &idi, const scimath::Axes &axes,
                 const casacore::IPosition &shape, const std::string partitionType="time");

    /// construct an object for a partition
    /// @param visInfo a shared_ptr to the associated VisInfo
    VisData(boost::shared_ptr<VisInfo> &visInfo, const scimath::Axes &axes,
                 const casacore::IPosition &shape);

    /// construct an object for a partition
    /// @param griddingInfo a shared_ptr to the associated GriddingInfo
    VisData(boost::shared_ptr<GriddingInfo> &griddingInfo, const scimath::Axes &axes,
                 const casacore::IPosition &shape);

    ~VisData();

    typedef boost::shared_ptr<VisData> ShPtr;

    /// @brief The number of data partitions (aka chunks) to iterate over
    /// @details The input DataAccessor is typically iterated over in time
    /// partitions, but the VisData may be sorted into other
    /// partition chunks (e.g. w-plane, oversampling plane, uv-block, etc.)
    /// @return the number partitions
    int nPartitions() {return itsNumberOfPartitions;}

    /// Return a reference to VisInfo
    /// @param index index of data partition
    /// @return a reference to VisInfo
    boost::shared_ptr<VisInfo> &visInfo(const int index) {return itsVisInfo[index];}

    /// Return a reference to GriddingInfo
    /// @param index index of data partition
    /// @return a reference to GriddingInfo
    boost::shared_ptr<GriddingInfo> &griddingInfo(const int index) {return itsGriddingInfo[index];}

    /// @brief obtain the centre of the image
    /// @details This method extracts RA and DEC axes from itsAxes and
    /// forms a direction measure corresponding to the middle of each axis.
    /// @return direction measure corresponding to the image centre
    casacore::MVDirection getImageCentre() const;

    /// @brief obtain the tangent point
    /// @details For faceting all images should be constructed for the same tangent
    /// point. This method extracts the tangent point (reference position) from the
    /// coordinate system.
    /// @return direction measure corresponding to the tangent point
    casacore::MVDirection getTangentPoint() const;

    /// @brief get image polarisation information from itsAxes and itsShape
    casacore::Vector<casacore::Stokes::StokesTypes> getImagePols();

    /// @brief return padding factor
    /// @return current padding factor
    float inline paddingFactor() const { return itsPaddingFactor;}
 
    /// @brief return axes definition for image
    /// @return axes definition for image
    askap::scimath::Axes inline axes() const { return itsAxes;}
 
    /// @brief return shape of image
    /// @return shape of image
    casacore::IPosition inline shape() const { return itsShape;}

private:

    int itsNumberOfPartitions;

    // raw visibility metadata (and for now data)
    std::vector<boost::shared_ptr<VisInfo> > itsVisInfo;

    // gridding visibility metadata (and for now data)
    std::vector<boost::shared_ptr<GriddingInfo> > itsGriddingInfo;

    /// Axes definition for image
    askap::scimath::Axes itsAxes;

    /// Shape of image
    casacore::IPosition itsShape;
 
    /// @brief internal iamge padding factor
    float itsPaddingFactor;    

};

} // end of namespace askap

#endif // #ifndef ASKAP_FACTORY_GRIDDING_DATA_ACCESSOR_H
