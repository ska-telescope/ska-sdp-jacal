/// @file VisGridder.h
///
/// @brief Table-based visibility gridder/degridder
///
/// @author Tim Cornwell <tim.cornwell@csiro.au>
/// @author Daniel Mitchell <tim.cornwell@csiro.au>

#ifndef ASKAP_FACTORY_VISGRIDDER_H
#define ASKAP_FACTORY_VISGRIDDER_H

#include <factory/gridders/IVisGridder.h>

// Yandasoft includes
#include <gridding/FrequencyMapper.h>
#include <scimath/utils/PolConverter.h>

// std includes
#include <string>

#include <boost/shared_ptr.hpp>

// 3rd party includes
#include <casacore/casa/BasicSL/Complex.h>

namespace askap
{

    /// @brief Incomplete base class for table-based gridding of visibility data.
    ///
    /// TVG supports gridding of visibility data onto one of a number of grids
    /// using one of a number of gridding functions. After gridding the final
    /// grid may be assembled by summing the grids appropriately. The separate
    /// grids may be for separate pointings, or for separate W planes. The summation
    /// process may include Fourier transformation.
    ///
    /// The main work in derived classes is to provide the convolution function
    /// and stacking operations.
    ///
    /// The sensitivity will vary as a function of position within the image.
    /// This is calculated by direct evaluation.
    ///
    /// @ingroup gridding
    class VisGridder : virtual public IVisGridder
    {
  public:

      /// @brief Standard two dimensional gridding using a convolution function
      /// in a table
      VisGridder();
      /// @brief Standard two dimensional gridding using a convolution function
      /// in a table
      /// @param[in] overSample Oversampling (currently limited to <=1)
      /// @param[in] support Support of function
      /// @param[in] padding padding factor (default is 1, i.e. no padding)
      /// @param name Name of table to save convolution function into
      VisGridder(const int overSample, const int support, const float padding = 1.,
          const std::string& name=std::string(""));

      /// @brief copy constructor
      /// @details it is required to decouple arrays between the input object
      /// and the copy.
      /// @param[in] other input object
      VisGridder(const VisGridder &other);

      virtual ~VisGridder();
 
      /// @brief return padding factor
      /// @return current padding factor
      float inline paddingFactor() const { return itsPaddingFactor;}
  
      /// @brief set padding factor
      /// @param[in] padding new padding factor
      void inline setPaddingFactor(const float padding) { itsPaddingFactor = padding;}

      /// @brief Save to a table (for debugging)
      /// @param name Name of table
      void save(const std::string& name);

      /// @brief Initialise resources common to gridding and degridding
      /// @param visData visdata that have been initialised with specific grid metadata
      virtual void initialiseCommon(const VisData::ShPtr& visData);

      /// @brief Further initialisation specific to gridding
      /// @param axes axes specifications
      /// @param shape Shape of output image: u,v,pol,chan
      /// @param dopsf Make the psf?
      virtual void initialiseGrid(const VisData::ShPtr& visData, const bool dopsf=true, const bool dopcf=false);

      /// @brief Further initialisation specific to degridding
      /// @param axes axes specifications
      /// @param image Input image: cube: u,v,pol,chan
      virtual void initialiseDegrid(const VisData::ShPtr& visData, const casacore::Array<imtype>& image);

      /// @brief assign weights
      /// @param viswt shared pointer to visibility weights
      virtual void initVisWeights(const askap::synthesis::IVisWeights::ShPtr &viswt);

      /// @brief Make context-dependant changes to the gridder behaviour
      /// @param context context
      virtual void customiseForContext(const std::string &context);

      /// @brief Grid the visibility data.
      /// @param griddingInfo GriddingInfo object to work with
      virtual void grid(boost::shared_ptr<GriddingInfo>& griddingInfo);

      static void gridKernel(casacore::Matrix<casacore::Complex>& grid,
              casacore::Matrix<casacore::Complex>& convFunc,
              const casacore::Complex& cVis, const int iu,
              const int iv, const int support);

      /// @brief Degrid the visibility data.
      /// @param[in] griddingInfo GriddingInfo object to work with
      virtual void degrid(boost::shared_ptr<GriddingInfo>& griddingInfo);

      /// Degridding kernel
      static void degridKernel(casacore::Complex& cVis,
              const casacore::Matrix<casacore::Complex>& convFunc,
              const casacore::Matrix<casacore::Complex>& grid,
              const int iu, const int iv,
              const int support);

      /// Form the final output image
      /// @param out Output double precision image or PSF
      virtual void finaliseGrid(casacore::Array<imtype>& out);

      /// @brief Finalise
      virtual void finaliseDegrid();

      /// @brief Calculate weights image
      /// @details Form the sum of the convolution function squared,
      /// multiplied by the weights for each different convolution
      /// function. This is used in the evaluation of the position
      /// dependent sensitivity
      /// @param out Output double precision sum of weights images
      virtual void finaliseWeights(casacore::Array<imtype>& out);

      /// @brief store given grid
      /// @details This is a helper method for debugging, it stores the amplitude of a given
      /// grid into a CASA image (prior to FFT done as part of finaliseGrid)
      /// @param[in] name image name
      /// @param[in] numGrid number of the grid to store
      void storeGrid(const std::string &name, casacore::uInt numGrid) const;

      /// @brief set or reset flag forcing gridder to track weights per oversampling plane
      /// @details change itsTrackWeightPerOversamplePlane
      /// @param[in] flag new value of the flag
      void inline trackWeightPerPlane(const bool flag) { itsTrackWeightPerOversamplePlane = flag;}

      /// @brief set or reset flag switching gridder to do Parallactic angle rotation
      /// @details change itsPARotation
      /// @param[in] flag new value of the flag
      /// @param[in] if flag = true, use given angle + feed angle from feed table
      void inline doPARotation(const bool flag, const double angle)
      { itsPARotation = flag; itsPARotAngle = angle;}

      /// @brief set or reset flag switching gridder to swap the polarisations
      /// @details change itsSwapPols, note that currently swap only works when PA Rotation is active
      /// @param[in] flag new value of the flag
      void inline doSwapPols(const bool flag) { itsSwapPols = flag;}

      /// @brief set or reset flag telling gridder to clear the grid after use
      /// @details the grid can usually be cleared to save memory
      /// @param[in] flag new value of the flag
      void inline doClearGrid(const bool flag) { itsClearGrid = flag;}

      /// @brief set table name to store the CFs to
      /// @details This method makes it possible to enable writing CFs to disk in destructor after the
      /// gridder is created. The main use case is to allow a better control of this feature in the parallel
      /// environment (we don't want all workers to write CFs)
      /// @param[in] name table name to store the CFs to (or an empty string if CFs are not to be stored)
      void setTableName(const std::string &name) { itsName = name; }

      /// @brief check whether the model is empty
      /// @details A simple check allows us to bypass heavy calculations if the input model
      /// is empty (all pixels are zero). This makes sense for degridding only.
      /// @brief true, if the model is empty
      virtual bool isModelEmpty() const;
      /// @brief return the current grid
      /// @details essentially just returns the same grid that storeGrid can write out
      inline const casacore::Array<casacore::Complex> getGrid() const {return itsGrid[0];}

  protected:
      /// @brief helper method to check that the gridder has been unused so far
      /// @details Unused means no visibilities were either gridded or degridded
      /// using this instance
      /// @return true, if this gridder has not been used
      bool unusedGridder() const  { return (itsNumberGridded<1) && (itsNumberDegridded<1); }

      /// @brief helper method to print CF cache stats in the log
      /// @details This method is largely intended for debugging. It writes down
      /// to the log the support sizes/offsets for all convolution functions in the cache and
      /// summarises the memory taken up by this cache (per gridder).
      void logCFCacheStats() const;


      /// @brief shape of the grid
      /// @details The could be a number of grids indexed though gIndex (for each row,
      /// polarisation and channel). However, all should have exactly the same shape.
      /// @return the shape of grid owned by this gridder
      inline const casacore::IPosition& shape() const { return itsShape;}

      /// @brief initialise sum of weights
      /// @details We keep track the number of times each convolution function is used per
      /// channel and polarisation (sum of weights). This method is made virtual to be able
      /// to do gridder specific initialisation without overriding initialiseGrid.
      /// This method accepts no parameters as itsShape, itsNWPlanes, etc should have already
      /// been initialised by the time this method is called.
      virtual void initialiseSumOfWeights();

      /// @brief zero sum of weights
      /// @details This method just sets all values of the current itsSumWeights biffer to zero
      /// without resizing it. It is done like this for a better encapsulation.
      void inline zeroSumOfWeights() { itsSumWeights.set(0.); }

      /// @brief resize sum of weights
      /// @details This method is used inside initialiseSumOfWeights and its overrides in
      /// derived classes. It resizes itsSumWeights to a given number of convolution
      /// functions taking into account channels/polarisations according to itsShape.
      /// Moving this operation into the separate method allows better data encapsulation
      /// and tracking weights per oversampling plane or per convolution function depending
      /// on the user's choice.
      /// @param[in] numcf number of convolution functions in the cache (before oversampling)
      void resizeSumOfWeights(const int numcf);

      /// @brief obtain sum of weights cube
      /// @return const reference to the sum of weights cube
      inline const casacore::Cube<double>& sumOfWeights() const { return itsSumWeights;}

      /// @brief log unused spectral planes
      /// @details It is handy to write down the channel numbers into log if the sum of weights
      /// is zero, i.e. if we have no data for this particular channel. This method does it
      /// (it is called from the destructor, unless the gridder object didn't do any gridding).
      void logUnusedSpectralPlanes() const;

      /// @brief translate row of the sum of weights cube into convolution function plane
      /// @details If we are tracking weights per oversampling plane, the row of the sum of
      /// weights cube directly corresponds to the plane of the convolution function cache.
      /// Otherwise, there is a factor of oversampling squared. It is handy to encapsulate
      /// this functionality here, so all derived classes work do not need to make a
      /// distinction how the weights are tracked.
      /// @param[in] row row of the sum of weights cube
      inline int cfIndexFromSumOfWeightsRow(const int row) const
          { return itsTrackWeightPerOversamplePlane ? row : itsOverSample*itsOverSample*row; }

      inline int SumOfWeightsRowFromCfIndex(const int row) const
          { return itsTrackWeightPerOversamplePlane ? row : row/(itsOverSample*itsOverSample); }

      /// @brief helper method to initialise frequency mapping
      /// @details Derived gridders may override initialiseGrid and initialiseDegrid. Howerver,
      /// they still need to be able to initialise frequency axis mapping (between accessor channels
      /// and image cube), which is handled by a private member class. This method initialises the
      /// mapper using the content of itsShape and itsAxes, which should be set prior to calling this
      /// method.
      void initialiseFreqMapping();

      /// @brief helper method to set up cell size
      /// @details Similar action is required to calculate uv-cell size for gridding and degridding.
      /// Moreover, derived gridders may override initialiseGrid and initialiseDegrid and we don't want
      /// to duplicate the code up there. This method calculates uv-cell size for both ra and dec axes
      /// using coordinate information provided. This method also assigns passed axes parameter to itsAxes.
      /// @param[in] axes coordinate system (ra and dec axes are used).
      void initialiseCellSize(const scimath::Axes& axes);

      /// @brief gridder configured to calculate PSF?
      /// @details
      /// @return true if this gridder is configured to calculate PSF, false otherwise
      bool inline isPSFGridder() const { return itsDopsf; }

      /// @brief gridder configured to calculate PreConditioner Function?
      /// @details
      /// @return true if this gridder is configured to calculate PCF, false otherwise
      bool inline isPCFGridder() const { return itsDopcf; }

      /// @brief configure gridder to calculate PSF or residuals
      /// @details This method is expected to be called from overridden initialiseGrid method
      /// @param[in] dopsf if true, the gridder is configured to calculate PSF, otherwise
      /// a normal residual grid is calculated.
      void inline configureForPSF(bool dopsf) { itsDopsf = dopsf;}

      /// @brief configure gridder to calculate PreConditioner Function
      /// @details This method is expected to be called from overridden initialiseGrid method
      /// @param[in] dopcf if true, the gridder is configured to calculate PCF
      void inline configureForPCF(bool dopcf) { itsDopcf = dopcf;}

      /// @brief obtain the centre of the image
      /// @details This method extracts RA and DEC axes from itsAxes and
      /// forms a direction measure corresponding to the middle of each axis.
      /// @return direction measure corresponding to the image centre
      casacore::MVDirection getImageCentre() const;

      /// @brief obtain the tangent point
      /// @details For faceting all images should be constructed for the same tangent
      /// point. This method extracts the tangent point (reference position) from the
      /// coordinate system.
      /// @return direction measure corresponding to the tangent point
      casacore::MVDirection getTangentPoint() const;

      // data members should be made private in the future!

      /// Axes definition for image
      askap::scimath::Axes itsAxes;

      /// Shape of image
      casacore::IPosition itsShape;

      /// Cell sizes in wavelengths
      casacore::Vector<double> itsUVCellSize;

//temporary comment out
//private:
      /// @brief Sum of weights (axes are index, pol, chan)
      casacore::Cube<double> itsSumWeights;
protected:

      /// @brief Convolution function
      /// The convolution function is stored as a vector of arrays so that we can
      /// use any of a number of functions. The index is calculated by cIndex.
      std::vector<casacore::Matrix<casacore::Complex> > itsConvFunc;

      /// @brief Obtain offset for the given convolution function
      /// @details To conserve memory and speed the gridding up, convolution functions stored in the cache
      /// may have an offset (i.e. essentially each CF should be defined on a bigger support and placed at a
      /// pixel other than the centre of this support). This method returns this offset, which is the
      /// position of the peak of the given CF on a bigger support w.r.t. the centre of this support.
      /// The value of (0,0) means no offset from the centre (i.e. support is already centred).
      /// @param[in] cfPlane plane of the convolution function cache to get the offset for
      /// @return a pair with offsets for each axis
      /// @note if there is no offset defined for a given cfPlane (default behavior), this method returns (0,0)
      std::pair<int,int> getConvFuncOffset(int cfPlane) const;

      /// @brief initialise convolution function offsets for a given number of planes
      /// @details The vector with offsets is resized and filled with (0,0).
      /// @param[in] nPlanes number of planes in the cache
      void initConvFuncOffsets(size_t nPlanes);

      /// @brief Assign offset to a particular convolution function
      /// @details
      /// @param[in] cfPlane plane of the convolution function cache to assign the offset for
      /// @param[in] x offset in the first coordinate
      /// @param[in] y offset in the second coordinate
      /// @note For this method, cfPlane should be within the range [0..nPlanes-1].
      void setConvFuncOffset(int cfPlane, int x, int y);

      /// @brief assign a given offset to the CF plane
      /// @details

      /// Return the index into the convolution function for a given sample
      /// @param index Visibility sample index in the current GriddingInfo
      virtual int cIndex(int sample);

      /// Return the index into the grid for a sample
      /// @param index Visibility sample index in the current GriddingInfo
      virtual int gIndex(int sample);

      /// @brief Initialize the convolution function - this is the key function to override.
      /// @param[in] visInfo const VisInfo to work with
      virtual void initConvolutionFunction(const boost::shared_ptr<VisInfo>& visInfo) = 0;

      /// @brief Initialise the indices
      /// @param[in] visInfo const VisInfo to work with
      virtual void initIndices(const boost::shared_ptr<VisInfo>& visInfo) = 0;

      /// @brief Correct for gridding convolution function
      /// @param image image to be corrected
      virtual void correctConvolution(casacore::Array<imtype>& image) = 0;

      /// @brief Initialise the indices
      /// @param[in] visInfo const VisInfo to work with
      /// @param[output] griddingInfo const VisInfo to work with
      void initGridIndices(const boost::shared_ptr<VisInfo>& visInfo,
                           boost::shared_ptr<GriddingInfo>& griddingInfo);

      /// @brief Conversion helper function
      /// @details Copies in to out expanding double into complex values and
      /// padding appropriately if necessary (padding is more than 1)
      /// @param[out] out complex output array
      /// @param[in] in double input array
      /// @param[in] padding padding factor
      static void toComplex(casacore::Array<imtypeComplex>& out, const casacore::Array<imtype>& in,
                     const float padding = 1.);

      /// @brief Conversion helper function
      /// @details Copies real part of in into double array and
      /// extracting an inner rectangle if necessary (padding is more than 1)
      /// @param[out] out real output array
      /// @param[in] in complex input array
      /// @param[in] padding padding factor
      static void toDouble(casacore::Array<imtype>& out, const casacore::Array<imtypeComplex>& in,
                    const float padding = 1.);

      /// @brief set up itsStokes using the information from itsAxes and itsShape
      void initStokes();

      /// Support of convolution function
      int itsSupport;
      /// Oversampling of convolution function
      int itsOverSample;

      /// Is the model empty? Used to shortcut degridding
      bool itsModelIsEmpty;

      /// The grid is stored as a cube as well so we can index into that as well.
      std::vector<casacore::Array<casacore::Complex> > itsGrid;

  private:

      /// @brief return the table name to store the result to
      /// @details This method could probably be made private as it is not used outside
      /// this class
      /// @return table name to store the CFs to (or an empty string if CFs are not to be stored)
      std::string tableName() const { return itsName; }

      /// Name of table to save to
      std::string itsName;

      /// @brief assignment operator
      /// @details it is required to decouple arrays between the input object
      /// and the copy.
      /// @param[in] other input object
      /// @return reference to itself
      /// @note assignment operator is made private, so wouldn't need to support both copy constructor and
      /// assignment operator. In the case of inadvertent use, compiler should give an error
      VisGridder& operator=(const VisGridder &other);

      /// @brief polarisation frame for the grid
      /// @details Assumed to be the same for all elements of itsGrid vector.
      /// This field is filled in initialiseGrid or initialiseDegrid using the Axes
      /// object.
      casacore::Vector<casacore::Stokes::StokesTypes> itsStokes;

      /// Number of samples gridded
      double itsSamplesGridded;
      /// Number of samples degridded
      double itsSamplesDegridded;
      /// Number of flagged visibility vectors (all pols.)
      double itsVectorsFlagged;
      /// Number of visibility vectors flagged on |w|>wmax (all pols.)
      double itsVectorsWFlagged;
      /// Number of grid cells gridded
      double itsNumberGridded;
      /// Number of grid cells degridded
      double itsNumberDegridded;
      /// Time for Coordinates
      double itsTimeCoordinates;
      /// Time for convolution functions
      double itsTimeConvFunctions;
      /// Time for gridding
      double itsTimeGridded;
      /// Time for degridding
      double itsTimeDegridded;

      /// @brief is this gridder a PSF gridder?
      bool itsDopsf;
      /// @brief is this gridder a PreConditioner Function gridder?
      bool itsDopcf;
  
      /// @brief internal padding factor, 1 by default
      float itsPaddingFactor;    

      /// Convert buffered vis data and weights to the imaging polarisation frame
      /// @param[in] visInfo VisInfo object to work with.
      void convertPol(boost::shared_ptr<VisInfo>& visInfo);

      /// Generic grid/degrid - this is the heart of this framework. It should never
      /// need to be overridden
      /// @param[in] griddingInfo GriddingInfo object to work with.
      /// @param[in] forward true for the model to visibility transform (degridding),
      /// false for the visibility to dirty image transform (gridding)
      void generic(boost::shared_ptr<GriddingInfo>& griddingInfo, bool forward);

      /// Visibility Weights
      askap::synthesis::IVisWeights::ShPtr itsVisWeight;

      /// @brief mapping class between image planes and accessor channels
      /// @details Correspondence between planes of the image cube and accessor channels may be
      /// non-trivial. This class takes care of the mapping.
      askap::synthesis::FrequencyMapper itsFreqMapper;

      /// @brief offsets of convolution functions
      /// @details To conserve memory and speed the gridding up, convolution functions stored in the cache
      /// may have an offset (i.e. essentially each CF should be defined on a bigger support and placed at a
      /// pixel other than the centre of this support). This data field defines this offset, which is the
      /// position of the peak of the given CF on a bigger support w.r.t. the centre of this support.
      /// The value of (0,0) means no offset from the centre (i.e. support is already centred). If the index
      /// of CF is beyond the size of this vector, (0,0) offset is assumed. By default this vector is empty,
      /// which means no offset.
      std::vector<std::pair<int,int> > itsConvFuncOffsets;

      /// @brief true, if itsSumWeights tracks weights per oversampling plane
      bool itsTrackWeightPerOversamplePlane;

      /// Are we doing Parallactic angle rotation?
      bool itsPARotation;
      double itsPARotAngle;

      /// Are we swapping the polarisations?
      bool itsSwapPols;

      /// @brief view of the currently used 2d grid
      /// @details to avoid creation/destruction overheads we keep a matrix that
      /// gives a view of the currently used plane in itsGrid
      casacore::Matrix<casacore::Complex> its2dGrid;

      /// @brief keep track of visibility polarisations we are setup to handle
      casacore::Vector<casacore::Stokes::StokesTypes> itsVisPols;

      /// @brief the polarization converter
      scimath::PolConverter itsPolConv;

      /// @brief vectors to store image polarisation values and noise, and visibility values
      casacore::Vector<casacore::Complex> itsImagePolFrameVis, itsImagePolFrameNoise, itsPolVector;

      /// @brief keep track of current image channel and index into itsGrid
      int itsImageChan, itsGridIndex;

      /// @brief release grid memory in finalise(De)Grid
      bool itsClearGrid;

    };

}
#endif

