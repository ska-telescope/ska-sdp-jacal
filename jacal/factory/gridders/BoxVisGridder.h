/// @file BoxVisGridder.h
///
/// BoxVisGridder: Box-based visibility gridder.
///
/// @author Tim Cornwell <tim.cornwell@csiro.au>

#ifndef ASKAP_FACTORY_BOXVISGRIDDER_H
#define ASKAP_FACTORY_BOXVISGRIDDER_H

#include <factory/gridders/VisGridder.h>
#include <factory/gridders/VisInfo.h>

namespace askap
{

    /// @brief Minimal box-car convolution (aka nearest neighbour) gridder.
    ///
    /// @details It doesn't work well but it is fast and simple.
    /// @ingroup gridding
    class BoxVisGridder : public VisGridder
    {
      public:

        // Standard two dimensional box gridding
        BoxVisGridder();

        /// Clone a copy of this Gridder
        virtual IVisGridder::ShPtr clone();

        /// @brief static method to get the name of the gridder
        /// @details We specify parameters per gridder type in the parset file.
        /// This method returns the gridder name which should be used to extract
        /// a subset of parameters for createGridder method.
        static inline std::string gridderName() { return "Box";}

        /// @brief static method to create gridder
        /// @details Each gridder should have a static factory method, which is
        /// able to create a particular type of the gridder and initialise it with
        /// the parameters taken form the given parset. It is assumed that the
        /// method receives a subset of parameters where the gridder name is already
        /// taken out.
        /// @param[in] parset input parset file
        /// @return a shared pointer to the gridder instance
        static IVisGridder::ShPtr createGridder(const LOFAR::ParameterSet& parset);

        virtual ~BoxVisGridder();

        /// @brief Initialise the indices
        /// @param[in] visInfo const VisInfo to work with
        virtual void initIndices(const boost::shared_ptr<VisInfo>& visInfo);

        /// @brief Correct for gridding convolution function
        /// @param image image to be corrected
        virtual void correctConvolution(casacore::Array<imtype>& image);

      protected:
        /// Initialize convolution function
        /// @param[in] visInfo const VisInfo to work with
        virtual void initConvolutionFunction(const boost::shared_ptr<VisInfo>& visInfo);
    };

}
#endif
