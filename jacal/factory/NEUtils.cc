/// @file NEUtils.cc

// for logging
#define ASKAP_PACKAGE_NAME "NEUtils"
#include <string>

/// askap namespace
namespace askap {
/// @return version of the package
    std::string getAskapPackageVersion_NEUtils() {
        return std::string("NEUtils");
    }
} // namespace askap

/// The version of the package
#define ASKAP_PACKAGE_VERSION askap::getAskapPackageVersion_NEUtils()

#include <vector>
#include <mutex>



#include <daliuge/DaliugeApplication.h>
#include <factory/ApplicationUtils.h>
#include <factory/NEUtils.h>

// LOFAR ParameterSet
#include <Common/ParameterSet.h>
// LOFAR Blob
#include <Blob/BlobString.h>
#include <Blob/BlobOBufString.h>
#include <Blob/BlobIBufString.h>
#include <Blob/BlobOStream.h>
#include <Blob/BlobIStream.h>
#include <Blob/BlobArray.h>
#include <Blob/BlobSTL.h>

// Base ASKAP
#include <askap/AskapLogging.h>
#include <askap/AskapError.h>

// params helpers
#include <measurementequation/SynthesisParamsHelper.h>
#include <measurementequation/ImageParamsHelper.h>
#include <measurementequation/ImageFFTEquation.h>
#include <parallel/SynParallel.h>
#include <parallel/GroupVisAggregator.h>


#include <gridding/IVisGridder.h>
#include <gridding/VisGridderFactory.h>
#include <scimath/fitting/ImagingNormalEquations.h>

#include <scimath/fitting/Params.h>


#include <string.h>
#include <sys/time.h>

#include <boost/regex.hpp>
#include <casacore/casa/BasicSL.h>
#include <casacore/casa/aips.h>
#include <casacore/casa/OS/Timer.h>
#include <casacore/ms/MeasurementSets/MeasurementSet.h>
#include <casacore/ms/MeasurementSets/MSColumns.h>
#include <casacore/ms/MSOper/MSReader.h>
#include <casacore/casa/Arrays/ArrayIO.h>
#include <casacore/casa/iostream.h>
#include <casacore/casa/namespace.h>
#include <casacore/casa/Quanta/MVTime.h>



namespace askap {

int NEUtils::getNChan(LOFAR::ParameterSet& parset) {
  // Read from the configruation the list of datasets to process
  const vector<string> ms = getDatasets(parset);

  const casacore::MeasurementSet in(ms[0]);

  return casacore::ROScalarColumn<casacore::Int>(in.spectralWindow(),"NUM_CHAN")(0);

}

double NEUtils::getChanWidth(LOFAR::ParameterSet& parset, int chan) {
  //FIXME: Assumes all datasets have the same chanWidth
  const vector<string> ms = getDatasets(parset);
  const casacore::MeasurementSet in(ms[0]);
  const casacore::ROMSColumns srcCols(in);
  const casacore::ROMSSpWindowColumns& sc = srcCols.spectralWindow();
  int srow = sc.nrow()-1;
  return sc.chanWidth()(srow)(casacore::IPosition(1, chan));
}

double NEUtils::getFrequency(LOFAR::ParameterSet& parset, int chan, bool barycentre) {

  // this appears to be needed when running parallel in frequency. Impact on performance has not been checked.
ASKAP_LOGGER(logger, ".NEUtils.getFrequency");
  #ifndef ASKAP_PATCHED
    static std::mutex safety;
    std::lock_guard<std::mutex> guard(safety);
  #endif // ASKAP_PATCHED

  // Read from the configruation the list of datasets to process
  const vector<string> ms = getDatasets(parset);

  const casacore::MeasurementSet in(ms[0]);
  const casacore::ROMSColumns srcCols(in);

  const casacore::ROMSSpWindowColumns& sc = srcCols.spectralWindow();
  const casacore::ROMSFieldColumns& fc = srcCols.field();
  const casacore::ROMSObservationColumns& oc = srcCols.observation();
  const casacore::ROMSAntennaColumns& ac = srcCols.antenna();
  const casacore::ROArrayColumn<casacore::Double> times = casacore::ROArrayColumn<casacore::Double>(oc.timeRange());
  const casacore::ROArrayColumn<casacore::Double> ants = casacore::ROArrayColumn<casacore::Double>(ac.position());
  const casacore::uInt thisRef = casacore::ROScalarColumn<casacore::Int>(in.spectralWindow(),"MEAS_FREQ_REF")(0);

  casacore::MVDirection Tangent;
  casacore::Vector<casacore::MDirection> DirVec;
  casacore::MVEpoch Epoch;
  casacore::MPosition Position;

  DirVec = fc.phaseDirMeasCol()(0);
  Tangent = DirVec(0).getValue();

  // Read the position on Antenna 0
  Array<casacore::Double> posval;
  ants.get(0,posval,true);
  vector<double> pval = posval.tovector();

  MVPosition mvobs(Quantity(pval[0], "m").getBaseValue(),
  Quantity(pval[1], "m").getBaseValue(),
  Quantity(pval[2], "m").getBaseValue());

  Position = MPosition(mvobs,casacore::MPosition::ITRF);

  // Get the Epoch
  Array<casacore::Double> tval;
  vector<double> tvals;

  times.get(0,tval,true);
  tvals = tval.tovector();
  double mjd = tvals[0]/(86400.);
  casacore::MVTime dat(mjd);

  Epoch = MVEpoch(dat.day());
  int srow = sc.nrow()-1;
  if (barycentre == false) {
    return sc.chanFreq()(srow)(casacore::IPosition(1, chan));
  }
  else {
    // FIXME:
    // need to put the barycentreing in here - the logic is all in the AdviseDI
  }

  return 1.0;

}

std::vector<double> NEUtils::getFrequencies(LOFAR::ParameterSet& parset) {
    #ifndef ASKAP_PATCHED
    static std::mutex safety;
    std::lock_guard<std::mutex> guard(safety);
    #endif // ASKAP_PATCHED
    // completely ignoring barycentreing. Add it if needed
    const vector<string> ms = getDatasets(parset);
    const casacore::MeasurementSet in(ms[0]);
    const casacore::ROMSColumns srcCols(in);
    const casacore::ROMSSpWindowColumns& sc = srcCols.spectralWindow();
    const int srow = sc.nrow()-1;
    ASKAPASSERT(sc.chanFreq().ndim(srow) == 1);
    return sc.chanFreq()(srow).tovector();
}

// @todo should this be in ApplicationUtils?
int NEUtils::getRank(char *uid) {

    // FIXME: make this more robust

    char *token, *string, *tofree;

    tofree = string = strdup(uid);
    assert(string != NULL);

    // Pull apart the Unique ID and get the rank/iid/whatever
    char * sessionID = strsep(&string, "_");        // UTC, e.g. 2021-12-11T00:27:09
    char * logicalGraphID = strsep(&string, "_");   // ID of drop, e.g. -5
    char * contextID = strsep(&string, "_");        // branch/rank, e.g. 0/0
    if (contextID == NULL) {
      return -1;
    }
    string = strdup(contextID);

    char * branch = strsep(&string,"/");
    char * rank   = strsep(&string,"/");

    if (rank != NULL) {
      ASKAPCHECK(atoi(branch)==0, "How should ranks be handled when the contextID branch is "<<branch<<"?");
      return atoi(rank);
    }
    else {
      return -1;
    }

}

  void NEUtils::sendNE(askap::scimath::ImagingNormalEquations::ShPtr itsNe, dlg_output_info &output) {
      ASKAP_LOGGER(logger, ".NEUtils.sendNE");

      ASKAPCHECK(itsNe, "NormalEquations not defined");

      LOFAR::BlobString b1;
      LOFAR::BlobOBufString bob(b1);
      LOFAR::BlobOStream bos(bob);
      bos << *itsNe;
      size_t itsNeSize = b1.size();
      ASKAPCHECK(itsNeSize > 0, "Zero size NE");
      // first the size
      output.write((char *) &itsNeSize, sizeof(itsNeSize));
      // then the actual data
      output.write(b1.data(), b1.size());
  }

  void NEUtils::receiveNE(askap::scimath::ImagingNormalEquations::ShPtr itsNE, dlg_input_info &input) {
      ASKAP_LOGGER(logger, ".NEUtils.sendNE");
      ASKAPCHECK(itsNE, "NormalEquations not defined");
      size_t itsNESize;
      size_t n_read = input.read((char *) &itsNESize, sizeof(itsNESize));
      LOFAR::BlobString b1;
      b1.resize(itsNESize);
      n_read = input.read(b1.data(), itsNESize);
      ASKAPCHECK(n_read == itsNESize, "Unable to read NE of expected size");
      LOFAR::BlobIBufString bib(b1);
      LOFAR::BlobIStream bis(bib);
      bis >> *itsNE;
  }

  void NEUtils::sendParams(askap::scimath::Params::ShPtr Params, dlg_output_info &output) {

      LOFAR::BlobString b1;
      LOFAR::BlobOBufString bob(b1);
      LOFAR::BlobOStream bos(bob);
     
      bos << *Params;
     
      size_t ParamsSize = b1.size();
      ASKAPCHECK(ParamsSize > 0, "Zero size Params");
      // first the size
      output.write((char *) &ParamsSize, sizeof(ParamsSize));
      // then the actual data
      output.write(b1.data(), b1.size());

  }

  void NEUtils::receiveParams(askap::scimath::Params::ShPtr Params, dlg_input_info &input) {
      ASKAP_LOGGER(logger, ".NEUtils.receiveParams");
      ASKAPCHECK(Params, "Params not defined");
      size_t ParamsSize;
      size_t n_read = input.read((char *) &ParamsSize, sizeof(ParamsSize));
      LOFAR::BlobString b1;
      if (n_read == 0) {
          ASKAPLOG_WARN_STR(logger, "Nothing received on the input port - exiting sendParams");
          return;
      }
      //ASKAPCHECK(n_read > 0, "Nothing received on the input port");
      b1.resize(ParamsSize);
      n_read = input.read(b1.data(), ParamsSize);

      if (n_read == ParamsSize) {
        LOFAR::BlobIBufString bib(b1);
        LOFAR::BlobIStream bis(bib);
        bis >> *Params;
      }
  }

  void NEUtils::sendVisInfo(VisInfo::ShPtr visInfo, dlg_output_info &output) {

      LOFAR::BlobString b1;
      LOFAR::BlobOBufString bob(b1);
      LOFAR::BlobOStream bos(bob);

      bos << *visInfo;

      size_t PartitionSize = b1.size();
      ASKAPCHECK(PartitionSize > 0, "Zero size Partition");
      // first the size
      output.write((char *) &PartitionSize, sizeof(PartitionSize));
      // then the actual data
      output.write(b1.data(), b1.size());

  }

  void NEUtils::receiveVisInfo(VisInfo::ShPtr visInfo, dlg_input_info &input) {
      ASKAP_LOGGER(logger, ".NEUtils.receiveVisInfo");
      ASKAPCHECK(visInfo, "partition not defined");
      size_t PartitionSize;
      size_t n_read = input.read((char *) &PartitionSize, sizeof(PartitionSize));
      LOFAR::BlobString b1;
      if (n_read == 0) {
          ASKAPLOG_WARN_STR(logger, "Nothing received on the input port - exiting receiveVisInfo");
          return;
      }
      //ASKAPCHECK(n_read > 0, "Nothing received on the input port");
      b1.resize(PartitionSize);
      n_read = input.read(b1.data(), PartitionSize);

      if (n_read == PartitionSize) {
        LOFAR::BlobIBufString bib(b1);
        LOFAR::BlobIStream bis(bib);
        bis >> *visInfo;
      }
  }

  void NEUtils::sendGriddingInfo(GriddingInfo::ShPtr visInfo, dlg_output_info &output) {

      LOFAR::BlobString b1;
      LOFAR::BlobOBufString bob(b1);
      LOFAR::BlobOStream bos(bob);
     
      bos << *visInfo;
     
      size_t PartitionSize = b1.size();
      ASKAPCHECK(PartitionSize > 0, "Zero size Partition");
      // first the size
      output.write((char *) &PartitionSize, sizeof(PartitionSize));
      // then the actual data
      output.write(b1.data(), b1.size());

  }

  void NEUtils::receiveGriddingInfo(GriddingInfo::ShPtr visInfo, dlg_input_info &input) {
      ASKAP_LOGGER(logger, ".NEUtils.receiveGriddingInfo");
      ASKAPCHECK(visInfo, "partition not defined");
      size_t PartitionSize;
      size_t n_read = input.read((char *) &PartitionSize, sizeof(PartitionSize));
      LOFAR::BlobString b1;
      if (n_read == 0) {
          ASKAPLOG_WARN_STR(logger, "Nothing received on the input port - exiting receiveGriddingInfo");
          return;
      }
      //ASKAPCHECK(n_read > 0, "Nothing received on the input port");
      b1.resize(PartitionSize);
      n_read = input.read(b1.data(), PartitionSize);

      if (n_read == PartitionSize) {
        LOFAR::BlobIBufString bib(b1);
        LOFAR::BlobIStream bis(bib);
        bis >> *visInfo;
      }
  }

  void NEUtils::sendVis(std::vector<std::vector<std::complex<float> > > &vis, dlg_output_info &output) {

      LOFAR::BlobString b1;
      LOFAR::BlobOBufString bob(b1);
      LOFAR::BlobOStream bos(bob);
     
      bos << vis;
     
      size_t visSize = b1.size();
      ASKAPCHECK(visSize > 0, "Zero size vis vector");
      // first the size
      output.write((char *) &visSize, sizeof(visSize));
      // then the actual data
      output.write(b1.data(), b1.size());

  }

  void NEUtils::receiveVis(std::vector<std::vector<std::complex<float> > > &vis,
                           const int nSample, const int nPol, dlg_input_info &input) {
      ASKAP_LOGGER(logger, ".NEUtils.receiveVis");
      //ASKAPCHECK(vis, "vis vector not defined");
      size_t visSize;
      size_t n_read = input.read((char *) &visSize, sizeof(visSize));
      LOFAR::BlobString b1;
      if (n_read == 0) {
          ASKAPLOG_WARN_STR(logger, "Nothing received on the input port - exiting receiveGriddingInfo");
          return;
      }
      //ASKAPCHECK(n_read > 0, "Nothing received on the input port");
      b1.resize(visSize);
      n_read = input.read(b1.data(), visSize);

      vis.resize(nSample);
      for (uint i=0; i<nSample; ++i) {
          vis[i].resize(nPol);
      }

      if (n_read == visSize) {
        LOFAR::BlobIBufString bib(b1);
        LOFAR::BlobIStream bis(bib);
        bis >> vis;
      }
  }

  void NEUtils::setSpectralBand(LOFAR::ParameterSet& parset, const int rank) {

      ASKAP_LOGGER(logger, ".NEUtils.setSpectralBand");

      // check if any DALiuGE-specific parameters have been added
      std::string scatter_axis = parset.getString("Daliuge.Scatter.scatter_axis","none");
      int num_of_copies = parset.getInt("Daliuge.Scatter.num_of_copies",1);

      ASKAPCHECK(scatter_axis=="frequency" || num_of_copies==1,
          "Only frequency scatters are supported. You have "<<num_of_copies<<" copies on axis "<<scatter_axis);

      // Only worry about the scatter if the scatter axis if frequency
      const int freq_rank = scatter_axis.compare("frequency") == 0 ? rank : 0;
      const int num_ranks = scatter_axis.compare("frequency") == 0 ? num_of_copies : 1;

      if (num_ranks > 1) {
          ASKAPLOG_DEBUG_STR(logger, "Setting spectral scatter band for rank "<<freq_rank<<" of "<<num_ranks);
      }

      // check for yandasoft spectral parameters
      LOFAR::ParameterSet imgParset = parset.makeSubset("Cimager.");      
      const int nchan = NEUtils::getNChan(imgParset);
      boost::optional<int> nchanpercore;
      boost::optional<int> startchan;
      std::string param;
      param = "Cimager.nchanpercore";
      if (parset.isDefined(param)) {
          nchanpercore = parset.getInt(param);
          ASKAPLOG_DEBUG_STR(logger, "read "<<param<<"="<<*nchanpercore<<" from parset");
      }
      param = "Cimager.Channels";
      if (parset.isDefined(param)) {
          const std::vector<std::string> cstr = parset.getStringVector(param);
          ASKAPLOG_DEBUG_STR(logger, "read "<<param<<"="<<cstr<<" from parset");
          ASKAPCHECK(cstr.size() == 2, param<<" requires two elements");
          if (nchanpercore) {
              ASKAPCHECK(*nchanpercore == stoi(cstr[0]), "Cimager params Channels and nchanpercore are inconsistent");
          }
          nchanpercore = stoi(cstr[0]);
          if (cstr[1].compare("%w") == 0) {
              //ASKAPLOG_WARN_STR(logger, "Wild card in the Channel list");
              // this is the default behaviour and is dealt with below
          }
          else {
              if (num_ranks > 1) {
                  // this function may have already been called. If so, the following should hold
                  // If there is some other use case, remove this check
                  ASKAPCHECK(stoi(cstr[1]) == freq_rank * *nchanpercore,
                      "Specified fixed start channel does not match expectations for frequency scatter");
              }
              startchan = stoi(cstr[1]);
          }
      }

      if (!nchanpercore) {
          // Spread full band over available ranks
          // Options: 1. require that num_ranks is an integer factor of nchan, 2. allow some load imbalance,
          //          3. potentially leave some unprocessed channels at the top of the band, ...
          // option 1:
          //ASKAPCHECK(nchan % num_ranks == 0, nchan<<" channels does not divide evenly into "<<num_ranks<<" scatters");
          if (nchan % num_ranks > 0) {
              ASKAPLOG_WARN_STR(logger, nchan<<" channels do not divide evenly into "<<num_ranks<<" scatters");
              // throw or return?
              ASKAPTHROW(std::runtime_error, nchan<<" channels do not divide evenly into "<<num_ranks<<" scatters");
          }
          ASKAPASSERT(num_ranks > 0);
          nchanpercore = nchan / num_ranks;
      }
      if (!startchan) {
          startchan = freq_rank * *nchanpercore;
      }
      ASKAPCHECK(*nchanpercore + *startchan <= nchan, "startchan + nchanpercore > nchan");

      ASKAPLOG_INFO_STR(logger, "rank = "<<freq_rank<<" of "<<num_ranks<<
                                ", channels "<<*startchan<<"-"<<*startchan+*nchanpercore-1<<
                                " of "<<num_ranks * *nchanpercore<<" to be processed (from a total of "<<nchan<<")");

      param = "Cimager.Channels";
      std::ostringstream pstr;
      pstr<<"["<<*nchanpercore<<","<<*startchan<<"]";
      parset.replace(param, pstr.str().c_str());

      return;

  }

  void NEUtils::finaliseParameters(LOFAR::ParameterSet& parset, const int rank) {

      // Need to get some information from the input dataset
      // this is done in "prepare" in AdviseDI - need to get the minimum
      // set - or just throw an exception and make the user add
      // the info ....

      // test for missing image-specific parameters:

      // these parameters can be set globally or individually

      ASKAP_LOGGER(logger, ".NEUtils.finaliseParameters");

      // This is called early on by all the apps, so put the update here?
      ASKAPLOG_DEBUG_STR(logger, "Updating logger for rank "<<rank);
      ApplicationUtils::updateLoggerContext(rank);

      // Set or update param Cimager.Channels based on various other options that may or may not be set
      NEUtils::setSpectralBand(parset, rank);

      LOFAR::ParameterSet imgParset = parset.makeSubset("Cimager.");      
      const std::vector<double> freqs = NEUtils::getFrequencies(imgParset);

      const int nChanVis = freqs.size();
      const double minFreq = *std::min_element(freqs.begin(), freqs.end());
      const double maxFreq = *std::max_element(freqs.begin(), freqs.end());
      bool higherOrderTerms = false;

      string param;

      const vector<string> imageNames = parset.getStringVector("Cimager.Images.Names", false);

      for (size_t img = 0; img < imageNames.size(); ++img) {

          // First, copy universal image params to specific images if undefined
          param = "cellsize";
          if ( !parset.isDefined("Cimager.Images."+imageNames[img]+"."+param) ) {
              ASKAPCHECK(parset.isDefined("Cimager.Images."+param), param+" required in parset");
              const std::string str = parset.getString("Cimager.Images."+param);
              parset.add("Cimager.Images."+imageNames[img]+"."+param, str);
          }
          param = "shape";
          if ( !parset.isDefined("Cimager.Images."+imageNames[img]+"."+param) ) {
              ASKAPCHECK(parset.isDefined("Cimager.Images."+param), param+" required in parset");
              const std::string str = parset.getString("Cimager.Images."+param);
              parset.add("Cimager.Images."+imageNames[img]+"."+param, str);
          }
          param = "direction";
          if ( !parset.isDefined("Cimager.Images."+imageNames[img]+"."+param) ) {
              ASKAPCHECK(parset.isDefined("Cimager.Images."+param), param+" required in parset");
              const std::string str = parset.getString("Cimager.Images."+param);
              parset.add("Cimager.Images."+imageNames[img]+"."+param, str);
          }

          // Note that if we need individual drops to have their own nchan and frequency parameters (e.g.
          // if each copy of a scatter needs to output its own spectral cube rather than a combined one),
          // the Cimager.Channels parameter should be set properly after the setSpectralBand call above.
          // It will have the appropriate nchanpercore and startchan values for this rank, and the freqs
          // vector will have the corresponding frequency values in Hz.
          // Alternatively, if we use the nchanpercore parameter to restrict processing to a sub-cube, can
          // do something like this to get the sub-cube limits:
          //     std::string scatter_axis = parset.getString("Daliuge.Scatter.scatter_axis","none");
          //     int num_of_copies = parset.getInt("Daliuge.Scatter.num_of_copies",1);
          //     const int num_ranks = scatter_axis.compare("frequency") == 0 ? num_of_copies : 1;
          //     const double minFreq = freqs[0];
          //     const double maxFreq = freqs[num_ranks*nchanpercore-1];
          // 
          // Currently neither of these options are available and it is assumed that nchan and frequency
          // should be set up for the whole band.

          // if the number of image frequency channels undefined, set to 1.
          int nChan = 1;
          param = "Cimager.Images."+imageNames[img]+".nchan";
          if ( parset.isDefined(param)) {
              nChan = parset.getInt(param);
              ASKAPCHECK(nChan <= nChanVis, "Too many output channels specified. "<<nChan<<" > "<<nChanVis);
          }
          else {
              ASKAPLOG_WARN_STR(logger, "Setting " << param);
              std::ostringstream pstr;
              pstr<<nChan;
              parset.add(param, pstr.str().c_str());
              ASKAPLOG_INFO_STR(logger, "  Advising on parameter " << param << ": " << pstr.str().c_str());
          }

          // if freq is undefined, use the info from the ms.
          // don't set it up for the current channels if it is a sub-band rank. This is the final output.
          param = "Cimager.Images."+imageNames[img]+".frequency";
          if (!parset.isDefined(param)) {
              std::ostringstream pstr;
              if (nChan==1) {
                  const double aveFreq = 0.5*(minFreq+maxFreq);
                  pstr<<"["<<aveFreq<<","<<aveFreq<<"]";
              } else {
                  pstr<<"["<<minFreq<<","<<maxFreq<<"]";
              }
              ASKAPLOG_INFO_STR(logger, "  Advising on parameter " << param << ": " << pstr.str().c_str());
              parset.add(param, pstr.str().c_str());
          }

          // record if nterms is ever greater than 1
          param = "Cimager.Images."+imageNames[img]+".nterms";
          if (parset.getInt(param,1) > 1) higherOrderTerms = true;

      }

      if (higherOrderTerms) {
          param = "Cimager.visweights"; // set to "MFS" if unset and nTerms > 1
          if (!parset.isDefined(param)) {
              std::ostringstream pstr;
              pstr << "MFS";
              ASKAPLOG_INFO_STR(logger, "Advising on parameter " << param <<": " << pstr.str().c_str());
              parset.add(param, pstr.str().c_str());
          }

          param = "Cimager.visweights.MFS.reffreq"; // set to average frequency if unset and nTerms > 1
          if ((parset.getString("Cimager.visweights")=="MFS")) {
              // in this case assume that the entire band is being averaged together and set ave freq as ref
              if (!parset.isDefined(param)) {
                  std::ostringstream pstr;
                  const double aveFreq = 0.5*(minFreq+maxFreq);
                  pstr<<aveFreq;
                  ASKAPLOG_INFO_STR(logger, "  Advising on parameter " << param <<": " << pstr.str().c_str());
                  parset.add(param, pstr.str().c_str());
              }
          }
      }

      param = "Cimager.Images.restFrequency";
      if ( !parset.isDefined(param) ) {
          std::ostringstream pstr;
          // Only J2000 is implemented at the moment.
          pstr<<"HI";
          parset.add(param, pstr.str().c_str());
      }

      ASKAPLOG_DEBUG_STR(logger, "Done updating params");

      return;
  }

  std::vector<std::string> NEUtils::getDatasets(const LOFAR::ParameterSet& parset)
  {
      if (parset.isDefined("dataset") && parset.isDefined("dataset0")) {
          ASKAPTHROW(std::runtime_error,
              "Both dataset and dataset0 are specified in the parset");
      }

      // First look for "dataset" and if that does not exist try "dataset0"
      vector<string> ms;
      if (parset.isDefined("dataset")) {
          ms = parset.getStringVector("dataset", true);
      } else {
          string key = "dataset0";   // First key to look for
          long idx = 0;
          while (parset.isDefined(key)) {
              const string value = parset.getString(key);
              ms.push_back(value);

              LOFAR::ostringstream ss;
              ss << "dataset" << idx + 1;
              key = ss.str();
              ++idx;
          }
      }

      return ms;
  }

  void NEUtils::readModels(const LOFAR::ParameterSet& parset, const scimath::Params::ShPtr &pModel)
  {

    ASKAP_LOGGER(logger, ".NEUtils.readModels");
    ASKAPCHECK(pModel, "model is not initialised prior to call to SynParallel::readModels");

    const std::vector<std::string> sources = parset.getStringVector("sources.names");
    std::set<std::string> loadedImageModels;
    for (size_t i=0; i<sources.size(); ++i) {
       const std::string modelPar = std::string("sources.")+sources[i]+".model";
       const std::string compPar = std::string("sources.")+sources[i]+".components";
       // check that only one is defined
       ASKAPCHECK(parset.isDefined(compPar) != parset.isDefined(modelPar),
            "The model should be defined with either image (via "<<modelPar<<") or components (via "<<
             compPar<<"), not both");
       //
         if (parset.isDefined(modelPar)) {
             const std::vector<std::string> vecModels = parset.getStringVector(modelPar);
             int nTaylorTerms = parset.getInt32(std::string("sources.")+sources[i]+".nterms",1);
             ASKAPCHECK(nTaylorTerms>0, "Number of Taylor terms is supposed to be a positive number, you gave "<<
                       nTaylorTerms);
             if (nTaylorTerms>1) {
                 ASKAPLOG_WARN_STR(logger,"Simulation from model presented by Taylor series (a.k.a. MFS-model) with "<<
                             nTaylorTerms<<" terms is not supported");
                 nTaylorTerms = 1;
             }
             ASKAPCHECK((vecModels.size() == 1) || (int(vecModels.size()) == nTaylorTerms),
                  "Number of model images given by "<<modelPar<<" should be either 1 or one per taylor term, you gave "<<
                  vecModels.size()<<" nTaylorTerms="<<nTaylorTerms);
             synthesis::ImageParamsHelper iph("image."+sources[i]);
             // for simulations we don't need cross-terms
             for (int order = 0; order<nTaylorTerms; ++order) {
                  if (nTaylorTerms > 1) {
                      // this is an MFS case, setup Taylor terms
                      iph.makeTaylorTerm(order);
                      ASKAPLOG_INFO_STR(logger,"Processing Taylor term "<<order);
                  }
                  std::string model = vecModels[0];
                  if (vecModels.size() == 1) {
                      // only base name is given, need to add taylor suffix
                      model += iph.suffix();
                  }

                  if (std::find(loadedImageModels.begin(),loadedImageModels.end(),model) != loadedImageModels.end()) {
                      ASKAPLOG_INFO_STR(logger, "Model " << model << " has already been loaded, reusing it for "<< sources[i]);
                      if (vecModels.size()!=1) {
                          ASKAPLOG_WARN_STR(logger, "MFS simulation will not work correctly if you specified the same model "<<
                               model<<" for multiple Taylor terms");
                      }
                  } else {
                      ASKAPLOG_INFO_STR(logger, "Adding image " << model << " as model for "<< sources[i]
                                         << ", parameter name: "<<iph.paramName() );
                      // need to patch model to append taylor suffix
                      synthesis::SynthesisParamsHelper::loadImageParameter(*pModel, iph.paramName(), model);
                      loadedImageModels.insert(model);
                  }
             }
         } else {
             // loop through components
             ASKAPLOG_INFO_STR(logger, "Adding components as model for "<< sources[i] );
             const vector<string> compList = parset.getStringVector(compPar);
             for (vector<string>::const_iterator cmp = compList.begin(); cmp != compList.end(); ++cmp) {
                  ASKAPLOG_INFO_STR(logger, "Loading component " << *cmp << " as part of the model for " << sources[i]);
                  synthesis::SynthesisParamsHelper::copyComponent(pModel, parset,*cmp,"sources.");
              }
         }
    }
    ASKAPLOG_INFO_STR(logger, "Successfully read models");
  }

} // namespace
